import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import './NavigationBar.css'
import {NavigationBarElement} from './NavigationBarElement/NavigationBarElement';
import Stack from '@mui/material/Stack';
import {disconnect} from '../../utils/auth/authSlice';
import {useAppDispatch} from '../../utils/hooks/store';
import {USER} from '../../model/types';
import {isLevelAdmin} from '../../services/FunctionService';


export const NavigationBar = ({user}: {user: USER}) => {
    const dispatch = useAppDispatch();
    const logout = () => {
        dispatch(disconnect());
    }

    return (
        <Box sx={{height: '5%', backgroundColor: 'var(--cst-grey-black)', flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar sx={{ height: '5%', justifyContent: "space-between", backgroundColor: 'var(--cst-grey-black)' }}>
                    <Button color="inherit" sx={{width: '10px', height: '10px'}}>
                    </Button>

                    <div>
                        {isLevelAdmin(user.id) ? adminElements : internElements}
                    </div>
                    <div>
                        <NavigationBarElement
                            icon={'power-off'}
                            text={'Se déconnecter'}
                            onClick={() => logout()}
                        />
                    </div>
                </Toolbar>
            </AppBar>
        </Box>
    );
};

const adminElements = (
    <Stack direction="row" spacing={2}>
        <NavigationBarElement
            path={"/app/account"}
            icon={'groups'}
            text={'Mon compte'}
        />
        <NavigationBarElement
            path={"/app/users-management"}
            icon={'groups'}
            text={'Utilisateurs'}
        />
        <NavigationBarElement
            path={"/app/questionnaries-management"}
            icon={'edit'}
            text={'Questionnaires'}
        />
    </Stack>
);

const internElements = (
    <Stack direction="row" spacing={2}>
        <NavigationBarElement
            path={"/app/account"}
            icon={'groups'}
            text={'Mon compte'}
        />
        <NavigationBarElement
            path={"/app/questionnaries"}
            icon={'groups'}
            text={'Questionnaires'}
        />
    </Stack>
);

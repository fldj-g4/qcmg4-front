import * as React from 'react';
import {DataGrid, GridColDef} from '@mui/x-data-grid';
import {Box} from '@mui/material';
import {OBJ} from '../../../model/types';
import './Datatable.css';
import {find} from 'lodash'

type DatatableProps = {
    entities: any,
    selectEntities?: (entities: any) => void,
    columnTranslations: OBJ,
    columns: any
}

export const Datatable = (props: DatatableProps) => {
    const {
        entities,
        selectEntities,
        columns,
        columnTranslations
    } = props;
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const colTranslation = (column: string) => {
        if (columnTranslations && columnTranslations[column]) {
            return columnTranslations[column].toUpperCase();
        }
        return column.toUpperCase();
    }

    const select = (ids: any) => {
        selectEntities && selectEntities(
            entities.filter((ent: any) => ids.includes(ent.id))
        );
    }
    return (
        <div style={{height: 500, width: '100%'}}>
            <Box sx={{
                height: 500,
                width: '100%',
                '& .MuiCheckbox-root': {color: "#424242"},
                '& .Mui-checked svg': {color: "#3F51B5"},
            }}>
                <DataGrid
                    rows={entities}
                    columns={columns}
                    pageSize={rowsPerPage}
                    rowsPerPageOptions={[rowsPerPage]}
                    checkboxSelection
                    onSelectionModelChange={(ids) => select(ids)}
                />
            </Box>
        </div>
    )
}

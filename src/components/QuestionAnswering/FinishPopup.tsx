import {Popin} from '../shared/Popin/Popin';
import {Box, Button} from '@mui/material';
import {ThemeProvider} from '@mui/material/styles';
import {buttonTheme} from '../shared/Styles'

export const FinishPopup = (props : any) => {

    const handleClose = () => {
        props.setShow(false)
    }

    const handleReturn = () => {
        props.sendPart();
        props.navigate('/app/questionnaries');
    }

    const content =
        <Box component="form" sx={{'& .MuiTextField-root': { m: 1, width: '50ch'},}} noValidate autoComplete="off">
            <p>Félicitations! Vous avez terminé le questionnaire. Vous pouvez consulter vos parcours effectués sur un questionnaire en cliquant sur ce dernier puis sur "Afficher les parcours effectués".</p>
        </Box>

    const buttons = <ThemeProvider theme={buttonTheme}><Button variant="contained" onClick={handleReturn}>RETOUR À LA LISTE DES QUESTIONNAIRES</Button></ThemeProvider>

    return (
        <Popin open={props.show} handleClose={handleClose} title={"Finalisation du questionnaire"} content={content} buttons={buttons}></Popin>
    )

}

export default FinishPopup;
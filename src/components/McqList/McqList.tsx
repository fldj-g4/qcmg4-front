import React from 'react';
import {Box, ButtonGroup, TextField} from '@mui/material';
import {Search} from '@mui/icons-material';
import InputAdornment from '@mui/material/InputAdornment';
import {faList, faQuestion, faSyncAlt} from '@fortawesome/free-solid-svg-icons'
import {MCQ, OBJ, PARTICIPATION} from '../../model/types';
import {McqManagementService} from '../../services/McqManagementService';
import {ParticipationService} from '../../services/ParticipationService';
import {Loader} from '../shared/Loader';
import {ToolBarButton} from '../shared/ToolBarButton';
import Divider from '@mui/material/Divider';
import {ErrorPopup} from '../shared/sharedPopup/ErrorPopup';
import {LaunchMcqPopup} from './LaunchMcqPopup';
import {ParticipationPopup} from './ParticipationPopup';
import {DataGrid, GridColDef} from '@mui/x-data-grid';
import {formatDate} from "../../services/FunctionService";
import {useGetMcqQuery, useGetUsersQuery, useGetParticipationsQuery} from "../../services/qcmplusApi";
import {useNavigate} from "react-router-dom";


const columns = [
    {
        field: 'name',
        headerName: 'Nom',
        sortable: true,
        flex : 3
    },
    {
        field: 'description',
        headerName: 'Description',
        sortable: true,
        flex : 10
    },
    {
        field: 'lastEditionAt',
        headerName: 'Modifié le',
        sortable: true,
        valueGetter: (params: any) => formatDate(params.row.lastEditionAt) || '',
        flex : 2
    }
];

type McqListStates = {
    selectedEntities: OBJ[],
    mcqs: OBJ[],
    participations : OBJ[]
    isLoaded: boolean,
    isLaunchPopupOpen: boolean,
    selectedMcq: Partial<MCQ>,
    isError0PopupOpen: boolean,
    isParticipationPopupOpen : boolean, 
    nbQuestions : number
}
const McqList = () => {
    const {data : getMcq} = useGetMcqQuery();
    const {data : getParticipation} = useGetParticipationsQuery();
return <QcmList getMcq = {getMcq} getParticipation = {getParticipation} navigate={useNavigate()}/>
}

export default McqList;

class QcmList extends React.Component<any, McqListStates> {
    constructor(props: any) {
        super(props);
        this.state = {
            selectedEntities: [],
            mcqs: [],
            participations: [],
            isLoaded: false,
            isLaunchPopupOpen: false,
            selectedMcq: {},
            isError0PopupOpen:false,
            isParticipationPopupOpen : false,
            nbQuestions : 0
        }
    }

    refresh = async () => {
        this.props.refetch();
    }

    launchMcq = (selectedEntities : OBJ[]) => {
        if (selectedEntities.length === 1){
            this.setState({isLaunchPopupOpen: !this.state.isLaunchPopupOpen, selectedMcq: selectedEntities[0], nbQuestions:selectedEntities[0].questions.length})
        }
        else {
            this.setState({isError0PopupOpen: !this.state.isError0PopupOpen})
        }
    }

    dispCourses = (selectedEntities : OBJ[]) => {
        if (selectedEntities.length === 1){
            this.setState({isParticipationPopupOpen: !this.state.isParticipationPopupOpen, selectedMcq: selectedEntities[0]})
        }
        else {
            this.setState({isError0PopupOpen: !this.state.isError0PopupOpen})
        }
    }

    select = (ids : any) => {
        const idsSet = new Set(ids)
        const selectedRowData = this.props.getMcq.filter((row : any) => idsSet.has(row.id));
        this.setState({selectedEntities : selectedRowData});
      }

    getParticipationByMcqId = (mcqId : any) => {
        if(mcqId !== undefined){
            const participations = this.props.getParticipation;
            return participations.filter((participation : any) => participation.idMcq === mcqId);
        }
    }

    render() {
        const {mcqs, isLoaded, selectedEntities, isLaunchPopupOpen, selectedMcq, isError0PopupOpen, participations, isParticipationPopupOpen} = this.state;
        return (
            <div>
                <Box sx={{
                    boxShadow: 'var(--cst-shadow)',
                    height: '100%',
                    backgroundColor: 'var(--cst-white)',
                }}>
                    <ButtonGroup size="large" sx={{
                        width: '100%',
                        borderRadius: '0',
                        borderBottom: '1px solid grey',
                        '> button': {flexGrow: 1},
                    }}>
                        <ToolBarButton key="refresh" icon={faSyncAlt} text="RAFRAÎCHIR" onClick={() => this.refresh()}/>
                        <Divider orientation="vertical" flexItem />
                        <ToolBarButton key="two" icon={faQuestion} text="LANCER LE QUESTIONNAIRE" onClick={() => this.launchMcq(selectedEntities)}/>
                        <Divider orientation="vertical" flexItem />
                        <ToolBarButton key="add" icon={faList} text="AFFICHER LES PARCOURS EFFECTUÉS" onClick={() => this.dispCourses(selectedEntities)}/>
                    </ButtonGroup>

                    <TextField
                        sx={{
                            'svg': {color: 'grey'},
                            width: '100%',
                        }}
                        variant="standard"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Search/>
                                </InputAdornment>
                            ),
                        }}
                    />

                {
                    (this.props.isLoading) ? <Loader/> :
                        <div style={{ height: 500, width: '100%' }}>
                            <DataGrid
                                rows={this.props.getMcq}
                                columns={columns}
                                pageSize={10}
                                rowsPerPageOptions={[10]}
                                onSelectionModelChange={(ids) => this.select(ids)}
                                sx = {{'& .Mui-selected':{color: "#3F51B5"},}}
                            />
                        </div>
                }
                </Box>
                <LaunchMcqPopup
                    show={isLaunchPopupOpen}
                    setShow={(show:boolean) => this.setState({isLaunchPopupOpen : show})} 
                    mcqToLaunch={selectedMcq}
                    nbQuestions={this.state.nbQuestions}
                    navigate={this.props.navigate}
                ></LaunchMcqPopup>
                <ParticipationPopup
                    show={isParticipationPopupOpen}
                    setShow={(show:boolean) => this.setState({isParticipationPopupOpen : show})}
                    mcqName={selectedMcq.name}
                    participations={this.getParticipationByMcqId(selectedMcq.id)}
                ></ParticipationPopup>
                <ErrorPopup 
                    show={isError0PopupOpen} 
                    setShow={(show:boolean) => this.setState({isError0PopupOpen : show})} 
                    content={"Veuillez sélectionner un questionnaire avant de choisir cette option."}
                ></ErrorPopup>
            </div>
        );
    }
}

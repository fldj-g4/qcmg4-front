import React from 'react';
import {Grid} from '@mui/material';
import QuestionAnswering from '../../components/QuestionAnswering/QuestionAnswering';
import {useNavigate, useParams} from "react-router-dom"

const QuestionAnsweringPage = () => {
    return (
            <Grid container spacing={0}
                  sx={{height: '90%', color: 'black'}}
            >
                <Grid
                    item
                    xs={15}
                    p={5}
                >
                    <QuestionAnswering params={useParams()} navigate={useNavigate()}/>
                </Grid>
            </Grid>
    );
}
export default QuestionAnsweringPage;
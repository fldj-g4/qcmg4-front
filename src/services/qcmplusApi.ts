// Need to use the React-specific entry point to import createApi
import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react'
import {RootState} from '../store';
import {USER} from '../model/types';
import ls from 'localstorage-slim';

export interface UserResponse {
    user: USER
    token: string
}

export interface LoginRequest {
    password: string
    username: string
}

// Define a service using a base URL and expected endpoints
export const qcmplusApi = createApi({
    reducerPath: 'qcmplusApi',
    baseQuery: fetchBaseQuery({
        baseUrl: 'https://qcm-api-staging.herokuapp.com/',
        // baseUrl: 'http://localhost:5142/',
        prepareHeaders: (headers, {getState}) => {
            // By default, if we have a token in the store, let's use that for authenticated requests
            const token = (getState() as RootState).auth.token
            if (token) {
                headers.set('authorization', `Bearer ${token}`)
            }
            return headers
        },
    }),
    tagTypes: ['User', 'Mcq', 'Participation', 'Questions', 'Answers'],
    keepUnusedDataFor: 0,
    endpoints: (builder) => ({
        login: builder.mutation<UserResponse, LoginRequest>({
            query: (credentials) => ({
                url: 'authenticate',
                method: 'POST',
                body: credentials,
            }),
        }),

        /* USERS */

        getUsers: builder.query<USER[], void>({
            query: () => 'api/user',
            providesTags: ['User'],
        }),
        getUserById: builder.query<USER, number | undefined>({
            query: (id) => `api/user/${id}`,
        }),
        getUserByToken: builder.query({
            query: () => 'api/user/me',
        }),
        editUserById: builder.query<USER[], void>({
            query: () => 'api/user',
        }),
        addUser: builder.mutation<USER, USER>({
            query: (user) => ({
                url: 'api/user',
                method: 'POST',
                body: {...user, password: "passer"},
            }),
            invalidatesTags: ['User'],
        }),
        editUser: builder.mutation<USER, USER>({
            query: (user) => ({
                url: 'api/user',
                method: 'PUT',
                body: user,
            }),
            invalidatesTags: ['User'],
        }),
        deleteUsers: builder.mutation<any, any>({
            query: (users) => ({
                url: 'api/user/',
                method: 'DELETE',
                body: users,
            }),
            invalidatesTags: ['User'],
        }),

        /* MCQ */

        getMcq: builder.query<any, void>({
            query: () => 'api/mcq',
            providesTags: ['Mcq'],
        }),
        getMcqById: builder.query<any, any>({
            query: (id) => `api/mcq/${id}`,
            providesTags: ['Mcq'],
        }),
        postMcq: builder.mutation<any, any>({
            query: (mcq) => ({
                url: 'api/mcq',
                method: 'POST',
                body: mcq,
            }),
            invalidatesTags: ['Mcq'],
        }),
        editMcq: builder.mutation<any, any>({
            query: (mcq) => ({
                url: `api/mcq/${mcq.id}`,
                method: 'PUT',
                body: mcq,
            }),
            invalidatesTags: ['Mcq'],
        }),
        deleteMcqById: builder.mutation<any, any>({
            query: (mcq) => ({
                url: `api/mcq/${mcq.id}`,
                method: 'DELETE',
                body: mcq,
            }),
            invalidatesTags: ['Mcq'],
        }),

        /* PARTICIPATION */

        getParticipations: builder.query<any, void>({
            query: () => 'api/participation/me',
        }),
        postParticipation: builder.mutation<any, any>({
            query: (part) => ({
                url: 'api/participation',
                method: 'POST',
                body: part,
            }),
            invalidatesTags: ['Participation'],
        }),
        deleteParticipation: builder.mutation<any, any>({
            query: (part) => ({
                url: `api/participation/${part.id}`,
                method: 'DELETE',
                body: part,
            }),
            invalidatesTags: ['Participation'],
        }),
        getParticipationById: builder.query<any, void>({
            query: (id) => `api/participation/${id}`,
        }),

        /* Question */
        getQuestions: builder.query<any, void>({
            query: () => `api/questions`,
            providesTags: ['Questions'],
        }),
        getQuestionById: builder.query<any, void>({
            query: (id) => `api/questions/${id}`,
            providesTags: ['Questions'],
        }),
        postQuestions: builder.mutation<any, any>({
            query: (part) => ({
                url: 'api/questions',
                method: 'POST',
                body: part,
            }),
            invalidatesTags: ['Questions', 'Mcq'],
        }),
        deleteQuestion: builder.mutation<any, any>({
            query: (qst) => ({
                url: `api/questions/`,
                method: 'DELETE',
                body: qst,
            }),
            invalidatesTags: ['Questions', 'Mcq'],
        }),
        editQuestion: builder.mutation<any, any>({
            query: (qst) => ({
                url: `api/questions/${qst.id}`,
                method: 'PUT',
                body: qst,
            }),
            invalidatesTags: ['Questions'],
        }),
        // getPost: builder.query({
        //     query: (id) => ({ url: `post/${id}` }),
        //     // Pick out data and prevent nested properties in a ho²²ok or selector
        //     transformResponse: (response) => response.data,
        //     // `result` is the server response
        //     providesTags: (result, error, id) => [{ type: 'Post', id }],
        //     // trigger side effects or optimistic updates
        //     onQueryStarted(id, { dispatch, getState, extra, requestId, queryFulfilled, getCacheEntry, updateCachedData }) {},
        //     // handle subscriptions etc
        //     onCacheEntryAdded(id, { dispatch, getState, extra, requestId, cacheEntryRemoved, cacheDataLoaded, getCacheEntry, updateCachedData }) {},
        // }),

        /* Answers */
        postAnswer: builder.mutation<any, any>({
            query: (part) => ({
                url: 'api/answers',
                method: 'POST',
                body: part,
            }),
            invalidatesTags: ['Answers'],
        }),
    }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const {
    useLoginMutation,
    useGetUserByIdQuery,
    useGetUsersQuery,
    useAddUserMutation,
    useDeleteUsersMutation,
    useEditUserMutation,
    useGetUserByTokenQuery,
    useDeleteMcqByIdMutation,
    useDeleteParticipationMutation,
    useDeleteQuestionMutation,
    useEditMcqMutation,
    useEditQuestionMutation,
    useEditUserByIdQuery,
    useGetMcqQuery,
    useGetParticipationByIdQuery,
    useGetParticipationsQuery,
    useGetMcqByIdQuery,
    usePostMcqMutation,
    usePostParticipationMutation,
    usePostQuestionsMutation,
    usePostAnswerMutation
} = qcmplusApi;

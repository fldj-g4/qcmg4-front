import React from 'react';
import {Grid} from '@mui/material';
import AccountManagement from '../../components/AccountManagement/AccountManagement'

const AccountPage = () => {
    return (
            <Grid container spacing={0}
                  sx={{height: '90%', color: 'black'}}
            >
                <Grid
                    item
                    xs={15}
                    p={5}
                >
                    <AccountManagement/>
                </Grid>
            </Grid>
    );
}
export default AccountPage;
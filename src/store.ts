import {configureStore} from '@reduxjs/toolkit';
import authSlice from './utils/auth/authSlice';
import {qcmplusApi} from "./services/qcmplusApi";
import toastSlice from "./utils/features/toastSlice";
import modelSlice from "./utils/features/modelSlice";

const store = configureStore({
    reducer: {
        // Add the generated reducer as a specific top-level slice
        [qcmplusApi.reducerPath]: qcmplusApi.reducer,
        auth: authSlice,
        toast: toastSlice,
        model: modelSlice,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(qcmplusApi.middleware),
});

export default store;

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

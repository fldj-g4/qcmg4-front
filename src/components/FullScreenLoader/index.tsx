import {CircularProgress} from "@mui/material";
import Grid from "@mui/material/Grid";

export const FullScreenLoader = () => (
    <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justifyContent="center"
        style={{ minHeight: '100vh' }}
    >

        <Grid item xs={3}>
            <CircularProgress color="success" />
        </Grid>
    </Grid>
);

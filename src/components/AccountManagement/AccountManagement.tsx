import React, {useState} from 'react';
import {Box} from '@mui/material';
import ChangePasswordPopup from './ChangePasswordPopup';
import UnactivateAccountPopup from './UnactivateAccountPopup';
import './AccountManagement.css';
import {useAuth} from '../../utils/hooks/useAuth';
import {Loader} from '../shared/Loader';
import {isLevelAdmin} from '../../services/FunctionService';

const AccountManagement = () => {
    const [isChangePWPopupOpen, setIsChangePWPopupOpen] = useState(false);
    const [isUnactivateAccountPopupOpen, setIsUnactivateAccountPopupOpen] = useState(false);
    const {user, userLoader} = useAuth();

    const toggleChangePWPopup = () => {
        setIsChangePWPopupOpen(!isChangePWPopupOpen);
    }

    const toggleUnactivateAccountPopup = () => {
        setIsUnactivateAccountPopupOpen(!isUnactivateAccountPopupOpen);
    }

    return !user ? (<Loader/>) : (
        <div>
            <Box sx={{
                boxShadow: 'var(--cst-shadow)',
                height: 300,
                backgroundColor: 'var(--cst-white)',
            }}>
                <h1>Bienvenue {user.firstName} {user.lastName}!</h1>
                <p>{isLevelAdmin(user.id)
                    ? "Pour commencer, cliquez sur l'un des liens : \"Utilisateurs\" ou \"Questionnaires\" ci-dessus."
                    : "Pour commencer, cliquez sur le lien \"Questionnaires\" ci-dessus."}
                </p>
                <p>Depuis cette page vous pouvez :</p>
                <ul>
                    <li><a href="javascript:void(0);" onClick={toggleChangePWPopup}>Changer votre mot de passe</a></li>
                    <li><a href="javascript:void(0);" onClick={toggleUnactivateAccountPopup}>Désactiver votre compte</a>
                    </li>
                </ul>
            </Box>
            <ChangePasswordPopup
                show={isChangePWPopupOpen}
                setShow={(show: boolean) => setIsChangePWPopupOpen(show)}
            />
            <UnactivateAccountPopup
                show={isUnactivateAccountPopupOpen}
                setShow={(show: boolean) => setIsUnactivateAccountPopupOpen(show)}
            />
        </div>
    )

}

export default AccountManagement;
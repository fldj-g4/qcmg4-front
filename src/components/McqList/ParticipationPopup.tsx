import {Popin} from '../shared/Popin/Popin';
import {Button, List, ListItem, ListItemText} from '@mui/material';
import {PARTICIPATION} from '../../model/types';
import {ThemeProvider} from '@mui/material/styles';
import {buttonTheme} from '../shared/Styles'

export const ParticipationPopup = (props:any) => {

    const handleClose = () => {
        props.setShow(false)
    }
    
    const buttons = <ThemeProvider theme={buttonTheme}><Button variant="contained" onClick={handleClose}>RETOUR A LA LISTE DES QUESTIONNAIRES</Button></ThemeProvider>

    const convert = (time : number) => {
        let hours = Math.floor(time / 3600);
        time = time - hours * 3600;
        let mins = Math.floor(time / 60);
        let secs = time - mins * 60;
        if (hours > 0) {
            return hours + " heures, " + mins + " minutes et " + secs + " secondes"
        }
        return mins + " minutes et " + secs + " secondes"
    }

    const content = (props : any) => {
        if (props.participations !== undefined) {
            return <div>
                <p>Vous avez effectué {props.participations.length} parcours sur le questionnaire {props.mcqName} : </p>
                <List sx={{ width: '100%', bgcolor: 'background.paper', position: 'relative', overflow: 'auto', maxHeight: 150, '& ul': { padding: 0 },}} subheader={<li />}>
                    {props.participations.map((participation : PARTICIPATION) => (
                        <ListItem>
                            <ListItemText primary={" - le " + participation.date + " en " + convert(participation.duration) + "."}></ListItemText>
                        </ListItem>
                    ))}
                </List>
        </div>
        }
        else {
            return <p>Vous n'avez effectué aucun parcours sur le questionnaire {props.mcqName}. </p>
        }
    }

    return (
        <Popin open={props.show} handleClose={handleClose} title={'Parcours effectués sur le questionnaire ' + props.mcqName} content={content(props)} buttons={buttons}></Popin>
    )

}
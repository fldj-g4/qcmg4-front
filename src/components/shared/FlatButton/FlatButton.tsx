import React from 'react';
import './FlatButton.css'

export const FlatButton = (props: any) => {
    return <button className="btn" {...props}>
        {props.children}
    </button>
}
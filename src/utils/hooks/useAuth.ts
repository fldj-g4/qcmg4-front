import {useMemo} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {selectCurrentUser, selectToken} from '../auth/authSlice';
import {notifier as notif} from "../features/toastSlice";
import {qcmplusApi} from "../../services/qcmplusApi";
import {useAppDispatch} from "./store";
import {RootState} from "../../store";
import ls from "localstorage-slim";

export const fetchCurrentUser = (state: RootState) => {
    return qcmplusApi.endpoints.getUserByToken.select(1)(state)
}
export const useAuth = () => {
    const user = useSelector(selectCurrentUser)
    return useMemo(() => (user), [user])
}

export const useToast = () => {
    const notifier = useSelector(notif)
    return useMemo(() => (notifier), [notifier])
}

import React from 'react';
import {Box, ButtonGroup, TextField} from '@mui/material';
import {Search} from '@mui/icons-material';
import InputAdornment from '@mui/material/InputAdornment';
import {faEye, faPen, faPlusCircle, faQuestion, faSyncAlt, faTrash} from '@fortawesome/free-solid-svg-icons'
import {Datatable} from '../shared/Datable/Datatable';
import {MCQ, OBJ} from '../../model/types';
import {McqManagementService} from '../../services/McqManagementService';
import {CreateEditMcqPopup} from './CreateEditMcqPopup';
import {Loader} from '../shared/Loader';
import {ToolBarButton} from '../shared/ToolBarButton';
import Divider from '@mui/material/Divider';
import {ErrorPopup} from '../shared/sharedPopup/ErrorPopup';
import {DeletePopup} from '../shared/sharedPopup/DeletePopup';
import {DetailPopup} from '../shared/sharedPopup/DetailPopup';
import {
    useDeleteQuestionMutation,
    useDeleteUsersMutation,
    useGetMcqQuery,
    useGetUsersQuery
} from "../../services/qcmplusApi";
import {useNavigate} from "react-router-dom";
import {formatDate} from "../../services/FunctionService";


const columns = [
    {field: 'id', type: 'number', headerName: 'ID'},
    {
        field: 'name',
        headerName: 'Nom',
        sortable: true,
    },
    {
        field: 'description',
        headerName: 'Description',
        sortable: true,
    },
    {
        field: 'createdBy',
        headerName: 'Créé par',
        sortable: true,
    },
    {
        field: 'createdAt',
        headerName: 'Créé le',
        sortable: true,
        valueGetter: (params: any) => formatDate(params.row.createdAt) || '',
    },
    {
        field: 'lastEditionBy',
        headerName: 'Modifié par',
        sortable: true,
    },
    {
        field: 'lastEditionAt',
        headerName: 'Modifié le',
        sortable: true,
        valueGetter: (params: any) => formatDate(params.row.lastEditionAt) || '',
    }
];

type McqManagementStates = {
    selectedEntities: OBJ[],
    mcqs: OBJ[],
    isLoaded: boolean,
    isAddPopupOpen: boolean,
    isEditPopupOpen: boolean,
    selectedMcq: Partial<MCQ>,
    isError0PopupOpen: boolean,
    isError2PopupOpen: boolean,
    isDeletePopupOpen: boolean,
    isDetailPopupOpen: boolean
}

const McqManagement = () => <Managment {...useGetMcqQuery()} {...useDeleteQuestionMutation()} navigate={useNavigate()}/>;

export default McqManagement;

class Managment extends React.Component<any, McqManagementStates> {
    constructor(props: any) {
        super(props);
        this.state = {
            selectedEntities: [],
            mcqs: [],
            isLoaded: false,
            isAddPopupOpen: false,
            isEditPopupOpen: false,
            selectedMcq: {},
            isError0PopupOpen: false,
            isError2PopupOpen: false,
            isDeletePopupOpen: false,
            isDetailPopupOpen: false
        }
    }

    refresh = async () => {
        this.props.refetch();
    }

    toggleCreatePopup = () => {
        this.setState({isAddPopupOpen: !this.state.isAddPopupOpen})
    }

    editMcq = (selectedEntities: OBJ[]) => {
        switch (selectedEntities.length) {
            case 0 :
                this.setState({isError0PopupOpen: !this.state.isError0PopupOpen})
                break
            case 1 :
                this.setState({isEditPopupOpen: !this.state.isEditPopupOpen, selectedMcq: selectedEntities[0]})
                break
            default :
                this.setState({isError2PopupOpen: !this.state.isError2PopupOpen})
                break
        }
    }

    editQuestions = (selectedEntities: OBJ[]) => {
        switch (selectedEntities.length) {
            case 0 :
                this.setState({isError0PopupOpen: !this.state.isError0PopupOpen})
                break
            case 1 :
                this.props.navigate('/app/questions-management/' + selectedEntities[0].id)
                break
            default :
                this.setState({isError2PopupOpen: !this.state.isError2PopupOpen})
                break
        }
    }

    deleteMcq = (selectedEntities: OBJ[]) => {
        if (selectedEntities.length === 0) {
            this.setState({isError0PopupOpen: !this.state.isError0PopupOpen})
        } else {
            this.setState({isDeletePopupOpen: !this.state.isDeletePopupOpen})
        }
    }

    detailMcq = (selectedEntities: OBJ[]) => {
        switch (selectedEntities.length) {
            case 0 :
                this.setState({isError0PopupOpen: !this.state.isError0PopupOpen})
                break
            case 1 :
                this.setState({isDetailPopupOpen: !this.state.isDetailPopupOpen, selectedMcq: selectedEntities[0]})
                break
            default :
                this.setState({isError2PopupOpen: !this.state.isError2PopupOpen})
                break
        }
    }

    render() {
        const {
            isLoaded,
            selectedEntities,
            isAddPopupOpen,
            isEditPopupOpen,
            selectedMcq,
            isError0PopupOpen,
            isError2PopupOpen,
            isDeletePopupOpen,
            isDetailPopupOpen
        } = this.state;
        return (
            <div>
                <Box sx={{
                    boxShadow: 'var(--cst-shadow)',
                    height: '100%',
                    backgroundColor: 'var(--cst-white)',
                }}>
                    <ButtonGroup size="large" sx={{
                        width: '100%',
                        borderRadius: '0',
                        borderBottom: '1px solid grey',
                        '> button': {flexGrow: 1},
                    }}>
                        <ToolBarButton key="refresh" icon={faSyncAlt} text="RAFRAÎCHIR" onClick={() => this.refresh()}/>
                        <Divider orientation="vertical" flexItem/>
                        <ToolBarButton key="two" icon={faEye} text="CONSULTER"
                                       onClick={() => this.detailMcq(selectedEntities)}/>
                        <Divider orientation="vertical" flexItem/>
                        <ToolBarButton key="add" icon={faPlusCircle} text="AJOUTER"
                                       onClick={() => this.toggleCreatePopup()}/>
                        <Divider orientation="vertical" flexItem/>
                        <ToolBarButton key="edit_mcq" icon={faPen} text="MODIFIER LE QUESTIONNAIRE"
                                       onClick={() => this.editMcq(selectedEntities)}/>
                        <Divider orientation="vertical" flexItem/>
                        <ToolBarButton key="edit_quest" icon={faQuestion} text="MODIFIER LES QUESTIONS"
                                       onClick={() => this.editQuestions(selectedEntities)}/>
                        <Divider orientation="vertical" flexItem/>
                        <ToolBarButton key="del" icon={faTrash} text="SUPPRIMER"
                                       onClick={() => this.deleteMcq(selectedEntities)}/>
                    </ButtonGroup>

                    <TextField
                        sx={{
                            'svg': {color: 'grey'},
                            width: '100%',
                        }}
                        variant="standard"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Search/>
                                </InputAdornment>
                            ),
                        }}
                    />
                    {
                        (this.props.isLoading) ? <Loader/> :
                            <Datatable
                                entities={this.props.data}
                                selectEntities={(entities: OBJ[]) => this.setState({selectedEntities: entities})}
                                columnTranslations={McqManagementService.getTranslationMapping()}
                                columns={columns}
                            />
                    }
                </Box>
                <DetailPopup
                    show={isDetailPopupOpen}
                    setShow={(show: boolean) => this.setState({isDetailPopupOpen: show})}
                    title={(selectedMcq !== undefined) ? "Détails du questionnaire " + selectedMcq.name : ""}
                    entityToDetail={selectedMcq}
                    columnTranslations={McqManagementService.getTranslationMapping()}>
                </DetailPopup>
                <CreateEditMcqPopup
                    show={isAddPopupOpen}
                    setShow={(show: boolean) => this.setState({isAddPopupOpen: show})}
                    title="Ajouter un nouveau questionnaire">
                </CreateEditMcqPopup>
                <CreateEditMcqPopup
                    show={isEditPopupOpen}
                    setShow={(show: boolean) => this.setState({isEditPopupOpen: show})}
                    title={(selectedMcq !== undefined) ? "Édition du questionnaire " + selectedMcq.name : ""}
                    mcqToModif={selectedMcq}>
                </CreateEditMcqPopup>
                <ErrorPopup
                    show={isError0PopupOpen}
                    setShow={(show: boolean) => this.setState({isError0PopupOpen: show})}
                    content={"Veuillez sélectionner au moins un questionnaire avant de choisir cette option."}>
                </ErrorPopup>
                <ErrorPopup
                    show={isError2PopupOpen}
                    setShow={(show: boolean) => this.setState({isError2PopupOpen: show})}
                    content={"Veuillez ne sélectionner qu'un seul questionnaire avant de choisir cette option."}>
                </ErrorPopup>
                <DeletePopup
                    show={isDeletePopupOpen}
                    setShow={(show: boolean) => this.setState({isDeletePopupOpen: show})}
                    entitiesToDel={selectedEntities}
                    entityName={"questionnaire"}>
                    deleteCallBack={this.props.triggerMutation}
                </DeletePopup>
            </div>
        );
    }
}

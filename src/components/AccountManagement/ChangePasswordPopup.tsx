import React, {useState} from 'react';
import {Popin} from '../shared/Popin/Popin';
import {Box, Button, TextField} from '@mui/material';
import {ThemeProvider} from '@mui/material/styles';
import {buttonTheme, inputFieldsStyle} from '../shared/Styles'

export const ChangePasswordPopup = (props : any) => {

    const [oldPW, setOldPW] = useState("");
    const [newPW1, setNewPW1] = useState("");
    const [newPW2, setNewPW2] = useState("");
    const [notSamePWError, setNotSamePWError] = useState(false);


    const handleClose = () => {
        setNotSamePWError(false)
        props.setShow(false)
    }
    
    const handleConfirm = () => {
        if (newPW1 !== newPW2){
            setNotSamePWError(true)
        }else{
            setNotSamePWError(false)
        }
    }
    
    const content =
        <Box component="form" sx={{'& .MuiTextField-root': { m: 1, width: '50ch'},}} noValidate autoComplete="off">
            <TextField sx={inputFieldsStyle} type="password" required id="oldPWField" label="Ancien mot de passe" onChange={(event) => {setOldPW(event.target.value)}}/>
            <TextField sx={inputFieldsStyle} error={notSamePWError} helperText={notSamePWError? "Ces deux champs doivent être identiques" : ""} type="password" required id="newPWField1" label="Nouveau mot de passe" onChange={(event) => {setNewPW1(event.target.value)}}/>
            <TextField sx={inputFieldsStyle} error={notSamePWError} helperText={notSamePWError? "Ces deux champs doivent être identiques" : ""} type="password" required id="newPWField2" label="Répéter le nouveau mot de passe" onChange={(event) => {setNewPW2(event.target.value)}}/>
        </Box>
    
    const buttons = 
        <div>
            <ThemeProvider theme={buttonTheme}>
                <Button variant="outlined" onClick={handleClose}>ANNULER</Button>
                <Button sx={{marginLeft:1}} variant="contained" onClick={handleConfirm}>CONFIRMER</Button> 
            </ThemeProvider>
        </div>
    
    return (
          <Popin open={props.show} handleClose={handleClose} title={"Modification du mot de passe"} content={content} buttons={buttons}></Popin>
    )

}

export default ChangePasswordPopup;
import {OBJ, QUESTION} from '../model/types';

export const QuestionManagementService = {
    getQuestions: async (): Promise<QUESTION[]> => getQuestions(),
    add: (question: QUESTION) => add(question),
    remove: (question: QUESTION) => remove(question),
    edit: (question: QUESTION) => edit(question),
    seedObj: (questionnaryId : string) => seedObj(questionnaryId),
    getTranslationMapping: () => getTranslationMapping(),
}

const getQuestions = async (): Promise<QUESTION[]> => {
    return new Promise(resolve => setTimeout(resolve, 2000));
}
const add = async (question: QUESTION) => {
    return new Promise(resolve => setTimeout(resolve, 2000));
}
const remove = async (question: QUESTION) => {
    return new Promise(resolve => setTimeout(resolve, 2000));
}
const edit = async (question: QUESTION) => {
    return new Promise(resolve => setTimeout(resolve, 2000));
}

const seed = [
    [{
        id: "0",
        title: "Quelle est la couleur de la voiture jaune d'Henri Dès ?",
        ans1: "Rouge",
        ans2: "Jaune",
        ans3: "Vert",
        ans4: "Violet",
        ans5: "Bleu",
        good_ans: [2],
        isActive: true,
    },
    {
        id: "1",
        title: "Dans la liste suivante, quels sont les pays européens ?",
        ans1: "France",
        ans2: "Australie",
        ans3: "Japon",
        ans4: "Allemagne",
        ans5: "Irlande",
        good_ans: [1,4,5],
        isActive: false,
    },
    {
        id: "2",
        title: "Le mot 'mot' est-il un mot ?",
        ans1: "Oui",
        ans2: "Non",
        ans3: "",
        ans4: "",
        ans5: "",
        good_ans: [1],
        isActive: true,
    },
    {
        id: "3",
        title: "Parmi les formes de vies suivantes, quelles sont des animaux ?",
        ans1: "Le lapin",
        ans2: "L'ortie",
        ans3: "L'amanite tue-mouche'",
        ans4: "L'ours",
        ans5: "Le requin",
        good_ans: [1,4,5],
        isActive: true,
    }],
    [{
        id: "4",
        title: "Dans la liste suivante, quels sont les pays européens ?",
        ans1: "Suisse",
        ans2: "Togo",
        ans3: "Tunisie",
        ans4: "Costa Rica",
        ans5: "Argentine",
        good_ans: [1],
        isActive: true,
    },
    {
        id: "5",
        title: "Quelle est la couleur de la voiture violette d'Henri Dès ?",
        ans1: "Rouge",
        ans2: "Jaune",
        ans3: "Vert",
        ans4: "Violet",
        ans5: "Bleu",
        good_ans: [4],
        isActive: true,
    },
    {
        id: "6",
        title: "La phrase 'La phrase est une phrase.' est-elle une phrase ?",
        ans1: "Oui",
        ans2: "Non",
        ans3: "",
        ans4: "",
        ans5: "",
        good_ans: [1],
        isActive: true,
    },
    {
        id: "7",
        title: "Parmi les formes de vies suivantes, quelles sont des animaux ?",
        ans1: "Le COVID-19",
        ans2: "Le chien",
        ans3: "Le chat",
        ans4: "L'herbe",
        ans5: "La morue",
        good_ans: [2,3,5],
        isActive: false,
    }],
    [{
        id: "8",
        title: "La lettre 'e' est-elle une lettre ?",
        ans1: "Oui",
        ans2: "Non",
        ans3: "",
        ans4: "",
        ans5: "",
        good_ans: [1],
        isActive: true,
    },
    {
        id: "9",
        title: "Dans la liste suivante, quels sont les pays européens ?",
        ans1: "Chine",
        ans2: "Mongolie",
        ans3: "Madagascar",
        ans4: "Chili",
        ans5: "Mauritanie",
        good_ans: [],
        isActive: true,
    },
    {
        id: "a",
        title: "Quelle est la couleur de la voiture bleue d'Henri Dès ?",
        ans1: "Rouge",
        ans2: "Jaune",
        ans3: "Vert",
        ans4: "Violet",
        ans5: "Bleu",
        good_ans: [5],
        isActive: true,
    },
    {
        id: "b",
        title: "Parmi les formes de vies suivantes, quelles sont des animaux ?",
        ans1: "Le hibou",
        ans2: "Le chien",
        ans3: "Le chat",
        ans4: "La mouette",
        ans5: "La morue",
        good_ans: [1,2,3,4,5],
        isActive: false,
    }],
    [{
        id: "c",
        title: "La chiffre '3' est-il un chiffre ?",
        ans1: "Oui",
        ans2: "Non",
        ans3: "",
        ans4: "",
        ans5: "",
        good_ans: [1],
        isActive: true,
    },
    {
        id: "d",
        title: "Dans la liste suivante, quels sont les pays européens ?",
        ans1: "Royaume-Uni",
        ans2: "Allemagne",
        ans3: "Lituanie",
        ans4: "Grèce",
        ans5: "Slovaquie",
        good_ans: [1,2,3,4,5],
        isActive: true,
    },
    {
        id: "e",
        title: "Quelle est la couleur de la voiture orange d'Henri Dès ?",
        ans1: "Rouge",
        ans2: "Jaune",
        ans3: "Vert",
        ans4: "Violet",
        ans5: "Bleu",
        good_ans: [],
        isActive: true,
    },
    {
        id: "f",
        title: "Parmi les formes de vies suivantes, quelles sont des animaux ?",
        ans1: "Le chêne",
        ans2: "Le sequoia",
        ans3: "La fouine",
        ans4: "Le zèbre",
        ans5: "La pivoine",
        good_ans: [3,4],
        isActive: false,
    }]
]

const seedObj = (questionnaryId : string): Promise<QUESTION[]> => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            return resolve(seed[parseInt(questionnaryId, 16)]);
        }, 2000);
    });
}

const getTranslationMapping = (): OBJ => {
    return {
        title: 'intitulé',
        ans1: 'réponse 1',
        ans2: 'réponse 2',
        ans3: 'réponse 3',
        ans4: 'réponse 4',
        ans5: 'réponse 5',
        good_ans: 'bonne réponse',
        isActive: 'actif ?',
    }
}

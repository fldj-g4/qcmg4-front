import {createSlice} from "@reduxjs/toolkit";
import {RootState} from "../../store";

interface ToastState {
    open: boolean,
    message: string,
    severity: 'error' | 'warning' | 'info' | 'success',
}

const toastSlice = createSlice({
    name: 'toast',
    initialState: {
        open: false,
        message: '',
        severity: 'info'
    } as ToastState,
    reducers: {
        toastify(state, {payload}: any) {
            const {open, message, severity} = payload
            state.open = open;
            state.message = message;
            state.severity = severity;
        },
        closify(state) {
            state.open = false;
        }
    },
});

export const {toastify, closify} = toastSlice.actions;
export default toastSlice.reducer;

export const notifier = (state: RootState) => {
    const {open, message, severity} = state.toast;
    return {open, message, severity};
}
export type OBJ = {
    [key: string]: any
};

export type ORDER = 'asc' | 'desc';

export type LOADER = 'born' | 'idle' | 'pending' | 'error';

export type USER = {
    id: number,
    login: string,
    lastName: string,
    firstName : string,
    company : string,
    role: {
        id: number,
        name: string
    }
};

export type MCQ = {
    id: string,
    name: string,
    description: string,
    // created_by: USER,
    // last_edition_by: USER,
    created_by: string,
    last_edited_by: string,
    // created_at: Date,
    // last_edited_at: Date,
    created_at: string,
    last_edited_at: string,
    isActive: boolean
};

export type QUESTION = {
    id: string,
    title: string,
    ans1: string,
    ans2: string,
    ans3: string,
    ans4: string,
    ans5: string,
    good_ans: number[],
    isActive: boolean
}

export type PARTICIPATION = {
    id: string,
    // date: Date
    date : string,
    duration : number
}

export type COLUMN_TRANSLATION = {
    key: string,
    translation: string,
}

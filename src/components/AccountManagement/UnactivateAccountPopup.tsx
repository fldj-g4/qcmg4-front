import {Popin} from '../shared/Popin/Popin';
import {Box, Button} from '@mui/material';
import {ThemeProvider} from '@mui/material/styles';
import {buttonTheme} from '../shared/Styles'

export const UnactivateAccountPopup = (props : any) => {

    const handleClose = () => {
        props.setShow(false)
    }
    
    const handleConfirm = () => {
    }

    const content =
        <Box component="form" sx={{'& .MuiTextField-root': { m: 1, width: '50ch'},}} noValidate autoComplete="off">
            <h2>ATTENTION</h2>
            <p id="msgPopup">Vous êtes sur le point de désactiver votre compte. Une fois votre compte désactivé, vous ne pourrez plus vous connecter et l'utiliser, êtes-vous sûr de vouloir le désactiver ?</p>
        </Box>

    const buttons = 
        <div>
            <ThemeProvider theme={buttonTheme}>
                <Button variant="outlined" onClick={handleClose}>ANNULER</Button>
                <Button sx={{marginLeft:1}} variant="contained" color="warning" onClick={handleConfirm}>DÉSACTIVER LE COMPTE</Button> 
            </ThemeProvider>
        </div>

    return (
        <Popin open={props.show} handleClose={handleClose} title={"Désactivation du compte"} content={content} buttons={buttons}></Popin>
    )

}

export default UnactivateAccountPopup;
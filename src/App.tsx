import React, {useEffect} from 'react';
import {BrowserRouter as Router, Navigate, Route, Routes} from 'react-router-dom';
import './App.css';
import {LoginPage} from './pages/LoginPage/LoginPage';
import UserManagementPage from './pages/UserManagementPage/UserManagementPage';
import McqManagementPage from './pages/McqManagementPage/McqManagementPage';
import QuestionManagementPage from './pages/QuestionManagementPage/QuestionManagementPage';
import McqListPage from './pages/McqListPage/McqListPage';
import AccountPage from './pages/AccountPage/AccountPage';
import QuestionAnsweringPage from './pages/QuestionAnsweringPage/QuestionAnsweringPage';
import background from './assets/images/background.svg';
import {useAuth, useToast} from "./utils/hooks/useAuth";
import {NavigationBar} from "./components/NavigationBar/NavigationBar";
import {useSelector} from "react-redux";
import {selectCurrentUser} from "./utils/auth/authSlice";
import ls from "localstorage-slim";
import {Alert, Snackbar} from "@mui/material";
import {useAppDispatch} from "./utils/hooks/store";
import {closify} from "./utils/features/toastSlice";

const App = () => {
    ls.get('token')
    const {user} = useAuth()
    const dispatch = useAppDispatch();
    const {open, message, severity} = useToast();
    const closeToast = () => dispatch(closify());

    return (
        <>
            <div
                className="main-container"
                style={{backgroundImage: `url(${background})`}}
            >
                <Snackbar
                    open={open}
                    autoHideDuration={6000}
                    onClose={closeToast}
                    anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                >
                    <Alert onClose={closeToast} severity={severity} sx={{width: '300px', maxWidth: '300px'}}>
                        {message}
                    </Alert>
                </Snackbar>
                <Router>
                    {user && <NavigationBar user={user}/>}
                    <Routes>
                        <Route
                            path="/"
                            element={
                                <RequireAuth>
                                    <AccountPage/>
                                </RequireAuth>}
                        />
                        <Route
                            path="/app"
                            element={
                                <RequireAuth>
                                    <AccountPage/>
                                </RequireAuth>}
                        />
                        <Route
                            path="/app/account"
                            element={
                                <RequireAuth>
                                    <AccountPage/>
                                </RequireAuth>}
                        />
                        <Route
                            path="/app/users-management"
                            element={
                                <RequireAuth>
                                    <UserManagementPage/>
                                </RequireAuth>}
                        />
                        <Route
                            path="/app/questionnaries-management"
                            element={
                                <RequireAuth>
                                    <McqManagementPage/>
                                </RequireAuth>}
                        />
                        <Route
                            path="/app/questions-management/:id"
                            element={
                                <RequireAuth>
                                    <QuestionManagementPage/>
                                </RequireAuth>}
                        />
                        <Route
                            path="/app/questionnaries"
                            element={
                                <RequireAuth>
                                    <McqListPage/>
                                </RequireAuth>}
                        />
                        <Route
                            path="/app/questionnary:id"
                            element={
                                <RequireAuth>
                                    <QuestionAnsweringPage/>
                                </RequireAuth>}
                        />
                        <Route
                            path="/login"
                            element={
                                <LoginPage/>
                            }
                        />

                    </Routes>
                </Router>
            </div>

        </>
    );
}

function RequireAuth({children}: any) {

    const {user} = useAuth()
    return user
        ? children
        : <Navigate to="/login" replace/>;
}

export default App;
import {OBJ, USER} from '../model/types';

export const UsersManagementService = {
    getUsers: async (): Promise<USER[]> => getUsers(),
    add: (user: USER) => add(user),
    remove: (user: USER) => remove(user),
    edit: (user: USER) => edit(user),
    seedObj: () => seedObj(),
    getTranslationMapping: () => getTranslationMapping(),
}

const getUsers = async (): Promise<USER[]> => {
    return new Promise(resolve => setTimeout(resolve, 2000));
}
const add = async (user: USER) => {
    return new Promise(resolve => setTimeout(resolve, 2000));
}
const remove = async (user: USER) => {
    return new Promise(resolve => setTimeout(resolve, 2000));
}
const edit = async (user: USER) => {
    return new Promise(resolve => setTimeout(resolve, 2000));
}

const seedObj = (): Promise<USER[]> => {
    const seed = [
        {
            id: '0',
            login: 'gvasquez',
            lastName: 'Vazquez',
            firstName: 'Gertrude',
            email: 'gertrudevazquez@pivitol.com',
            company: 'société',
            isAdmin: true,
            isActive: true,
        },
        {
            id: '1',
            login: 'otucker',
            lastName: 'Tucker',
            firstName: 'Osborne',
            email: 'osbornetucker@pivitol.com',
            company: 'AFH',
            isAdmin: false,
            isActive: true,
        },
        {
            id: '2',
            login: 'jnoir',
            lastName: 'Noir',
            firstName: 'Justin',
            email: 'justinnoir@pivitol.com',
            company: '123FE',
            isAdmin: true,
            isActive: true,
        },
        {
            id: '3',
            login: 'oboufy',
            lastName: 'Boufy',
            firstName: 'Oscar',
            email: 'oscarboufy@pivitol.com',
            company: 'KillBastards',
            isAdmin: false,
            isActive: false,
        },
        {
            id: '4',
            login: 'mlory',
            lastName: 'Lory',
            firstName: 'Mary',
            email: 'marylory@pivitol.com',
            company: 'FHHG',
            isAdmin: true,
            isActive: true,
        },
        {
            id: '5',
            login: 'sconnor',
            lastName: 'Connor',
            firstName: 'Sara',
            email: 'saraconnor@pivitol.com',
            company: 'Basté',
            isAdmin: false,
            isActive: false,
        },
        {
            id: '6',
            login: 'gbridou',
            lastName: 'Bridou',
            firstName: 'Gertrude',
            email: 'gertrudebridou@pivitol.com',
            company: 'BridouSA',
            isAdmin: true,
            isActive: false,
        },
        {
            id: '7',
            login: 'mblanc',
            lastName: 'Blanc',
            firstName: 'Michel',
            email: 'michelblanc@pivitol.com',
            company: 'Entreprise',
            isAdmin: true,
            isActive: false,
        },
        {
            id: '8',
            login: 'sjohnson',
            lastName: 'Johnson',
            firstName: 'Scarlett',
            email: 'scarlettjohnson@pivitol.com',
            company: 'BridouSA',
            isAdmin: true,
            isActive: false,
        },
        {
            id: '9',
            login: 'narmstrong',
            lastName: 'Armstrong',
            firstName: 'Niels',
            email: 'nielsarmstrong@pivitol.com',
            company: 'NASA',
            isAdmin: true,
            isActive: false,
        },
        {
            id: 'a',
            login: 'jbezos',
            lastName: 'Bezos',
            firstName: 'Jeff',
            email: 'jeffbezos@pivitol.com',
            company: 'Amazon',
            isAdmin: true,
            isActive: false,
        },
        {
            id: 'b',
            login: 'aeinstein',
            lastName: 'Einstein',
            firstName: 'Albert',
            email: 'alberteinstein@pivitol.com',
            company: 'EinsteinSA',
            isAdmin: true,
            isActive: false,
        },
    ]
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            // return resolve(seed);
        }, 200);
    });
}

const getTranslationMapping = (): OBJ => {
    return {
        lastName: 'nom',
        firstName: 'prénom',
        email : 'e-mail',
        company: 'société',
        isAdmin : 'admin ?',
        isActive: 'actif ?',
        role : "rôle"
    }
}
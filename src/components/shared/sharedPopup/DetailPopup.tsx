import {Popin} from '../Popin/Popin'
import {Button, List, ListItem, ListItemText} from '@mui/material';
import {buttonTheme} from '../Styles';
import {ThemeProvider} from '@mui/material/styles';
import { formatDate } from '../../../services/FunctionService';

export const DetailPopup = (props:any) => {

    const handleClose = () => {
        props.setShow(false)
      }

    const colTranslation = (column : string) => {
      if (props.columnTranslations && props.columnTranslations[column]) {
          return props.columnTranslations[column].toUpperCase();
      }
      return column.toUpperCase();
    }

    const content = (entityToDetail : any) => {
      if (entityToDetail != undefined) {
        return(
          <List sx={{ width: '100%', bgcolor: 'background.paper', position: 'relative', overflow: 'auto', maxHeight: 200, '& ul': { padding: 0 },}} subheader={<li />}>
          {Object.entries(entityToDetail).map((entry : any) => (
          <ListItem>
            {entry[0] != "role" && entry[0] != "createdAt" && entry[0] != "lastEditionAt" ? <ListItemText primary={colTranslation(entry[0]) + " : " + entry[1]}></ListItemText>
              : entry[0] == "role" ? <ListItemText primary={colTranslation(entry[0]) + " : " + entry[1].name}></ListItemText>
              : <ListItemText primary={colTranslation(entry[0]) + " : " + formatDate(entry[1])}></ListItemText>}
          </ListItem>
        ))}
        </List>
        )
      }
    }
      

    const buttons = <ThemeProvider theme={buttonTheme}><Button variant="contained" onClick={handleClose}>RETOUR</Button></ThemeProvider>

    return (
      <Popin open={props.show} handleClose={handleClose} title={props.title} content={content(props.entityToDetail)} buttons={buttons}></Popin>
    )
}

export default DetailPopup;
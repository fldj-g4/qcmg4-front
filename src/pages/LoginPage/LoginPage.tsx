import * as React from 'react';
import './LoginPage.css';
import Grid from '@mui/material/Grid';
import {LoginForm} from '../../components/LoginForm/LoginForm';

export const LoginPage = () => {
    return (
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="center"
            style={{minHeight: '100vh'}}
        >
            <Grid item xs={3}>
                <LoginForm/>
            </Grid>
        </Grid>
    );
}

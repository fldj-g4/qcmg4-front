import {createSlice} from "@reduxjs/toolkit";
import {RootState} from "../../store";
import {USER} from "../../model/types";

interface ModelState {
    users: USER[],
}

const usersSlice = createSlice({
    name: 'model',
    initialState: {
        users: [],
    } as ModelState,
    reducers: {
        setUsers(state, payload: any) {
            state.users = payload;
        },
    },
});

export const {setUsers} = usersSlice.actions;
export default usersSlice.reducer;

export const allUsers = (state: RootState) => {
    const {users} = state.model;
    return {users};
}
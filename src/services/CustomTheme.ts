import {createTheme} from '@mui/material';
import {lightBlue} from '@mui/material/colors';

export const CustomTheme = createTheme({
    palette: {
        primary: {
            main: '#fff',

        },
        secondary: {
            main: lightBlue[500],
        },
    },
});

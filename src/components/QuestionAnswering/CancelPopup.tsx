import {Popin} from '../shared/Popin/Popin';
import {Box, Button} from '@mui/material';
import {ThemeProvider} from '@mui/material/styles';
import {buttonTheme} from '../shared/Styles'

export const CancelPopup = (props : any) => {

    const handleClose = () => {
        props.setShow(false)
    }
    
    const handleReturn = () => {
        props.sendPart();
        props.navigate('/app/questionnaries')
    }

    const content =
        <Box component="form" sx={{'& .MuiTextField-root': { m: 1, width: '50ch'},}} noValidate autoComplete="off">
            <p>Si vous continuez, vous sortirez de la procédure de questionnaire. Vous ne pourrez pas reprendre où vous vous êtes arrêté et votre parcours sera sauvegardé dans la base de données. Vous pourrez cependant recommencer ce questionnaire.</p>
        </Box>

    const buttons = 
        <div>
            <ThemeProvider theme={buttonTheme}>
                <Button variant="outlined" onClick={handleClose}>RETOUR AU QUESTIONNAIRE</Button>
                <Button sx={{marginLeft:1}} variant="contained" color="warning" onClick={handleReturn}>ANNULER LE QUESTIONNAIRE</Button> 
            </ThemeProvider>
        </div>

    return (
        <Popin open={props.show} handleClose={handleClose} title={"Annulation du questionnaire"} content={content} buttons={buttons}></Popin>
    )

}

export default CancelPopup;
import React from 'react';
import {Box, Button, Checkbox} from '@mui/material';
import {QuestionManagementService} from '../../services/QuestionManagementService';
import {OBJ, QUESTION} from '../../model/types';
import {CancelPopup} from './CancelPopup';
import {FinishPopup} from './FinishPopup';
import {ThemeProvider} from '@mui/material/styles';
import {buttonTheme, checkBoxStyle} from '../shared/Styles'
import {useGetMcqByIdQuery, usePostAnswerMutation, usePostParticipationMutation} from "../../services/qcmplusApi";
import {useNavigate} from "react-router-dom";
import { checkGridRowIdIsValid } from '@mui/x-data-grid';
import { useAuth } from '../../utils/hooks/useAuth';

type QuestionAnsweringStates = {
    questions: OBJ[],
    question : Partial<QUESTION>,
    index : number,
    isLoaded: boolean,
    isCancelPopupOpen: boolean,
    isFinishPopupOpen: boolean,
    checked: boolean[],
    answers : OBJ[]
}

const QuestionAnswering = (props: any) => {
    const [addAnswer, {isLoading: addALoading}] = usePostAnswerMutation();
    const [addParticipation, {isLoading: addPLoading}] = usePostParticipationMutation();
    const {user} = useAuth(); 
    return <QA {...useGetMcqByIdQuery(props.params.id)} postAnswer = {addAnswer} postParticipation = {addParticipation} user = {user} {...props}/>;
}

export default QuestionAnswering;

class QA extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            questions: [],
            question: {},
            index : 0,
            isLoaded: false,
            isCancelPopupOpen: false,
            isFinishPopupOpen: false,
            checked: [false, false, false, false, false],
            answers: []
        }
    }

    questionDisp = (question : Partial<QUESTION>) => {
        let disp = []
        if (!this.props.isLoading) {
            let question = this.props.data.questions[this.state.index]
            let answers = Object.values(question).slice(2, 7)
                disp.push(<Box sx={boxStyle}>{question.question}</Box>)
                let i = 0
                while(answers[i] !== undefined && answers[i] !== ""){
                    switch (i) {
                        case 0 :
                            disp.push(
                                <Box sx={boxStyle}>
                                    <Checkbox 
                                    checked={this.state.checked[i]}
                                    onChange={(event) => {this.setState({checked : [event.target.checked, this.state.checked[1], this.state.checked[2], this.state.checked[3], this.state.checked[4]]})}}
                                    sx={checkBoxStyle}/>
                                    {answers[i]}
                                </Box>
                            )
                            i++
                            continue;
                        case 1 : 
                            disp.push(
                                <Box sx={boxStyle}>
                                    <Checkbox 
                                    checked={this.state.checked[i]}
                                    onChange={(event) => {this.setState({checked : [this.state.checked[0], event.target.checked, this.state.checked[2], this.state.checked[3], this.state.checked[4]]})}}
                                    sx={checkBoxStyle}/>
                                    {answers[i]}
                                </Box>
                            )
                            i++
                            continue;
                        case 2 : 
                            disp.push(
                                <Box sx={boxStyle}>
                                    <Checkbox 
                                    checked={this.state.checked[i]}
                                    onChange={(event) => {this.setState({checked : [this.state.checked[0], this.state.checked[1], event.target.checked, this.state.checked[3], this.state.checked[4]]})}}
                                    sx={checkBoxStyle}/>
                                    {answers[i]}
                                </Box>
                            )
                            i++
                            continue;
                        case 3 : 
                            disp.push(
                                <Box sx={boxStyle}>
                                    <Checkbox 
                                    checked={this.state.checked[i]}
                                    onChange={(event) => {this.setState({checked : [this.state.checked[0], this.state.checked[1], this.state.checked[2], event.target.checked, this.state.checked[4]]})}}
                                    sx={checkBoxStyle}/>
                                    {answers[i]}
                                </Box>
                            )
                            i++
                            continue;
                        case 4 : 
                            disp.push(
                                <Box sx={boxStyle}>
                                    <Checkbox 
                                    checked={this.state.checked[i]}
                                    onChange={(event) => {this.setState({checked : [this.state.checked[0], this.state.checked[1], this.state.checked[2], this.state.checked[3], event.target.checked]})}}
                                    sx={checkBoxStyle}/>
                                    {answers[i]}
                                </Box>
                            )
                            i++
                            continue;
                    }
                }
            }
            return <div>{disp}</div>
        }

    buttons = () => {
        if (!this.props.isLoading) {
            let buttons = [<Button variant="outlined" color="warning" onClick={this.handleCancel}>ANNULER LE QUESTIONNAIRE</Button>]
            // if (this.state.question.id !== undefined && (this.state.questions.indexOf(this.state.question) === this.props.data.questions.length() - 1)) {
            if (this.state.index == this.props.data.questions.length - 1){
                buttons.push(<Button variant="contained" onClick={this.handleFinish}>TERMINER LE QUESTIONNAIRE</Button>)
            } else {
                buttons.push(<Button variant="outlined" onClick={this.handleNext}>QUESTION SUIVANTE</Button>)
            }
            return (
                <div><ThemeProvider theme={buttonTheme}>{buttons}</ThemeProvider></div>
            )
            }
    }

    handleCancel = () => {
        this.setState({isCancelPopupOpen: !this.state.isCancelPopupOpen})
    }
    
    handleNext = async () => {
        let answerList : any[] = [];
        this.state.checked.forEach((check : any) => {
            if (check) {
                answerList.push(1);
            }else{
                answerList.push(0);
            }
        });
        const ansToSend = {
            id: 0,
            answer : answerList.toString(),
            idQuestion : this.props.data.questions[this.state.index].id
        }
        this.props.postAnswer(ansToSend).unwrap().then((payload : any) => {
            const tempAns = [...this.state.answers];
            tempAns.push(payload);
            this.setState({checked : [false, false, false, false, false], answers : tempAns, index : this.state.index + 1});
        })
    }

    sendParticipation = () => {
        const partToSend = {
            answers : this.state.answers,
            date : new Date(),
            duration : 0,
            idUser: this.props.user.id,
            idMcq : this.props.params.id
        } 
        this.props.postParticipation(partToSend);
    }

    handleFinish = () => {
        this.setState({isFinishPopupOpen: !this.state.isFinishPopupOpen})
    }

    render() {
        const {questions, isLoaded, question, isCancelPopupOpen, isFinishPopupOpen} = this.state;
        return(
            <div>
                <Box sx={boxStyle}>
                    <Box sx={boxStyle}>Pour chacune des questions qui s'affichent, veuillez cocher toutes les réponses justes et cliquer sur "Valider" pour confirmer votre réponse.</Box>
                    {this.questionDisp(question)}
                    <Box sx={boxStyle}>{this.buttons()}</Box>
                </Box>
                <CancelPopup
                    show={isCancelPopupOpen}
                    setShow={(show: boolean) => this.setState({isCancelPopupOpen: show})}
                    navigate = {this.props.navigate}
                    sendPart = {this.sendParticipation}
                />
                <FinishPopup
                    show={isFinishPopupOpen}
                    setShow={(show: boolean) => this.setState({isFinishPopupOpen: show})}
                    navigate = {this.props.navigate}
                    sendPart = {this.sendParticipation}
                />
            </div>
        )
    }

}

const boxStyle = {
    boxShadow: 'var(--cst-shadow)',
    height: '100%',
    backgroundColor: 'var(--cst-white)',
}

import React from 'react';
import {Grid} from '@mui/material';
import McqManagement from '../../components/McqManagement/McqManagement';
import {useNavigate} from "react-router-dom";

const McqManagementPage = () => {
    return (
            <Grid container spacing={0}
                  sx={{height: '90%', color: 'black'}}
            >
                <Grid
                    item
                    xs={15}
                    p={5}
                >
                    <McqManagement/>
                </Grid>
            </Grid>
    );
}
export default McqManagementPage;
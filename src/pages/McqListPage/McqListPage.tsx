import React from 'react';
import {Grid} from '@mui/material';
import McqList from '../../components/McqList/McqList';
import {useNavigate} from "react-router-dom";

const McqListPage = () => {
    return (
            <Grid container spacing={0}
                  sx={{height: '90%', color: 'black'}}
            >
                <Grid
                    item
                    xs={15}
                    p={5}
                >
                    <McqList/>
                </Grid>
            </Grid>
    );
}
export default McqListPage;
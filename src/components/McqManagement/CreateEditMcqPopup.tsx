import {Popin} from '../shared/Popin/Popin'
import {Box, Button, TextField} from '@mui/material';
import {ThemeProvider} from '@mui/material/styles';
import {buttonTheme, inputFieldsStyle} from '../shared/Styles'
import {useState} from "react";
import {useEditMcqMutation, usePostMcqMutation} from "../../services/qcmplusApi";
import {notify} from "../../services/FunctionService";

export const CreateEditMcqPopup = (props: any) => {

    const [nameField, setNameField] = useState(props.mcqToModif?.name || '');
    const [descField, setDescField] = useState(props.mcqToModif?.description || '');

    const [postMcq, {isLoading: addLoading}] = usePostMcqMutation();
    const [editMcq, {isLoading: editLoading}] = useEditMcqMutation();

    const handleClose = () => {
        props.setShow(false)
    }

    const handleConfirm = () => {
        const successToast = ({name}: any) => {
            notify(
                `Le questionnaire ${name} a été ${props.mcqToModif ? 'modifié' : 'créé'} !`,
                'success'
            );
        }
        const warningToast = (error: any) => {
            notify(error.message, 'error')
        };
        const action = props.mcqToModif ? editMcq({
            id: props?.mcqToModif?.id,
            description: descField,
            name: nameField
        }) : postMcq({description: descField, name: nameField});

        action.unwrap()
            .then((payload) => {
                successToast(payload);
                handleClose();
            })
            .catch((error) => warningToast(error))
    }

    const content = (mcqToModif : any) => {
        return (
            <Box component="form" sx={{'& .MuiTextField-root': {m: 1, width: '50ch'},}} noValidate autoComplete="off">
                <TextField sx={inputFieldsStyle} required id="nameField" label="Nom"
                                defaultValue={mcqToModif?.name} onChange={(e) => setNameField(e.target.value)}/>
                <TextField sx={inputFieldsStyle} required id="descField" label="Description" multiline rows={2}
                                defaultValue={mcqToModif?.description} onChange={(e) => setDescField(e.target.value)}/>
            </Box>
        )
    }


    const buttons =
        <div>
            <ThemeProvider theme={buttonTheme}>
                <Button variant="outlined" onClick={handleClose}>ANNULER</Button>
                <Button variant="contained" onClick={handleConfirm}>CONFIRMER</Button>
            </ThemeProvider>
        </div>

    return (
        <Popin open={props.show} handleClose={handleClose} title={props.title} content={content(props.mcqToModif)}
               buttons={buttons}/>
    )
}

export default CreateEditMcqPopup;
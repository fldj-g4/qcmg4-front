import * as React from 'react';
import {Box, IconButton} from '@mui/material';
import {LockOutlined, PersonOutline, Visibility, VisibilityOff} from '@mui/icons-material';
import Grid from '@mui/material/Grid';
import {LoadingButton} from '@mui/lab';
import CssTextField from '../shared/CssTextField';
import './LoginForm.css';
import {useNavigate} from 'react-router-dom';
import {LoginRequest, useLoginMutation} from '../../services/qcmplusApi';
import {useState} from "react";
import {notify} from "../../services/FunctionService";
import ls from "localstorage-slim";

export const LoginForm = (props: any) => {
    const navigate = useNavigate();
    const [login, {isLoading}] = useLoginMutation()
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [showPassword, setShowPassword] = useState(false)
    const [isError, setIsError] = useState(false)

    const handleClick = async () => {
        const formState = {
            password,
            username,
        } as LoginRequest;

        await login(formState).unwrap()
            .then(() => {navigate('/');}
            )
            .catch(() => setIsError(true));

    }

    const enter = ({keyCode}: any) => {
        if (keyCode === 13 && username !== '' && password !== '') {
            handleClick();
        }
    };

    return (
        <Box
            sx={{
                width: 300,
                height: 300,
                maxWidth: '100%',
                backgroundColor: 'var(--cst-fade-black)',
                borderRadius: 5,
                display: 'flex',
                alignItems: 'center',
                p: 3,
            }}
        >
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justifyContent="center"
            >
                <CssTextField
                    fullWidth
                    placeholder="Identifiant"
                    id="identifiant"
                    margin="normal"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    onKeyUp={enter}
                    InputProps={{
                        startAdornment: <PersonOutline className="icon"/>,
                    }}
                    size="small"
                    color="secondary"
                    error={isError}
                />

                <CssTextField
                    fullWidth
                    placeholder="Mot de passe"
                    id="mdp"
                    margin="normal"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    onKeyUp={enter}
                    type={showPassword ? 'text' : 'password'}
                    InputProps={{
                        startAdornment: <LockOutlined className="icon"/>,
                        endAdornment: <PasswordIcon
                            onClick={() => setShowPassword(!showPassword)}
                            showPassword={showPassword}/>,
                    }}
                    size="small"
                    color="info"
                    error={isError}
                />

                <LoadingButton
                    size="medium"
                    color="primary"
                    onClick={handleClick}
                    loading={isLoading}
                    variant="contained"
                    sx={{
                        width: '100%',
                        mt: 5
                    }}
                >
                    Se connecter
                </LoadingButton>
            </Grid>
        </Box>
    );
}

const PasswordIcon = ({onClick, showPassword}: { onClick: () => void, showPassword: boolean }) => (
    <IconButton
        aria-label="Changer la visibilité du mot de passe"
        onClick={onClick}
        edge="start"
        size="small"
        className="password-icon"
    >
        {showPassword ? <VisibilityOff/> : <Visibility/>}
    </IconButton>
);


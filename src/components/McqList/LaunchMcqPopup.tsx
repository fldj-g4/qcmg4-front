import {Popin} from '../shared/Popin/Popin';
import {Box, Button} from '@mui/material';
import {ThemeProvider} from '@mui/material/styles';
import {buttonTheme} from '../shared/Styles'

export const LaunchMcqPopup = (props : any) => {

    const handleClose = () => {
        props.setShow(false)
    }
    
    const handleConfirm = () => {
        props.navigate('/app/questionnary'+ props.mcqToLaunch.id)
    }

    const content = 
        <Box component="form" sx={{'& .MuiTextField-root': { m: 1, width: '50ch'},}} noValidate autoComplete="off">
            <p>Vous êtes sur le point de lancer le questionnaire {props.mcqToLaunch !== undefined ? props.mcqToLaunch.name : ""}, ce dernier comprend {props.nbQuestions !== undefined ? props.nbQuestions : ""} questions. Une fois confirmation, vous devrez répondre à ces {props.nbQuestions !== undefined ? props.nbQuestions : ""} questions sans pouvoir faire de pause. Êtes-vous sûr de vouloir le lancer ?</p>
        </Box>

    const buttons = 
        <div>
            <ThemeProvider theme={buttonTheme}>
                <Button variant="outlined" onClick={handleClose}>ANNULER</Button>
                <Button sx={{marginLeft:1}} variant="contained" onClick={handleConfirm}>CONFIRMER</Button> 
            </ThemeProvider>
        </div>
        

    return (
        <Popin open={props.show} handleClose={handleClose} title={"Confirmation"} content={content} buttons={buttons}></Popin>
    )

}

export default LaunchMcqPopup;
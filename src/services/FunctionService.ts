import {OBJ} from '../model/types';
import {toastify} from "../utils/features/toastSlice";
import {useAppDispatch} from "../utils/hooks/store";
import store from "../store";

export const FunctionService = {
    descendingComparator: <T>(a: T, b: T, orderBy: keyof T) => descendingComparator(a,b,orderBy),
    sortEntities: (entities: OBJ[], criteria: string, order: boolean) => sortEntities(entities, criteria, order),
};

const sortEntities = (entities: OBJ[], criteria: string, order: boolean) => {
    if (!entities || entities.length === 0 || criteria === '') return entities;
    const orderSign = order ? '+' : '-';
    return entities.sort((a, b) => {
        let result = 0
        if (a[criteria] > b[criteria]) {
            result = 1
        } else if (a[criteria] < b[criteria]) {
            result = -1;
        }
        return Math.sign(Number(`${orderSign}1`)) * result;
    })
}

const descendingComparator = <T>(a: T, b: T, orderBy: keyof T) => {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

export const isLevelAdmin = (id: number) => {
    return id === 1 || id === 2;
}

export const notify = (message: string, severity: string) => {
    store.dispatch(toastify({
        open: true,
        message: message,
        severity: severity
    }))
}

export const roles = [{id: 1, name: 'SUPERADMIN'},  {id: 2, name: 'ADMIN'}, {id: 3, name: 'USER'}]

export const formatDate = (date : string) => {
    let year = date.substring(0,4);
    let month = date.substring(5,7);
    let day = date.substring(8,10);
    return day + "/" + month + "/" + year;
}
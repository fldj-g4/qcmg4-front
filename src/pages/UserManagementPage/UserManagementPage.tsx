import React from 'react';
import {Grid} from '@mui/material';
import UserManagement from '../../components/UserManagement/UserManagement';

const UserManagementPage = () => {
    return (
            <Grid container spacing={0}
                  sx={{height: '90%', color: 'black'}}
            >
                <Grid
                    item
                    xs={15}
                    p={5}
                    mt={4}
                >
                    <UserManagement/>
                </Grid>
            </Grid>
    );
}
export default UserManagementPage;
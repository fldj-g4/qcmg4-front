import {Popin} from '../shared/Popin/Popin'
import {Box, Button, Checkbox, TextField} from '@mui/material';
import {ThemeProvider} from '@mui/material/styles';
import {buttonTheme, checkBoxStyle, inputFieldsStyle} from '../shared/Styles'
import {useState} from "react";
import {usePostQuestionsMutation, useEditQuestionMutation} from "../../services/qcmplusApi";
import {notify} from "../../services/FunctionService";

export const CreateEditQuestionPopup = (props: any) => {
    const [question, setQuestion] = useState(props.questionToModif?.title || "")
    const [ans1Field, setans1Field1] = useState(props.questionToModif?.ans1 || "")
    const [ans2Field, setans1Field2] = useState(props.questionToModif?.ans2 || "")
    const [ans3Field, setans1Field3] = useState(props.questionToModif?.ans3 || "")
    const [ans4Field, setans1Field4] = useState(props.questionToModif?.ans4 || "")
    const [ans5Field, setans1Field5] = useState(props.questionToModif?.ans5 || "")
    const [checkboxes, setCheckboxes] = useState({
        as1: props.questionToModif?.good_ans?.includes(1) || false,
        as2: props.questionToModif?.good_ans?.includes(2) || false,
        as3: props.questionToModif?.good_ans?.includes(3) || false,
        as4: props.questionToModif?.good_ans?.includes(4) || false,
        as5: props.questionToModif?.good_ans?.includes(5) || false,
    })

    const [postQuestion, {isLoading: postLoading}] = usePostQuestionsMutation();
    const [editQuestion, {isLoading: editLoading}] = useEditQuestionMutation();

    const handleCheck = (val: any) => {
        setCheckboxes({...checkboxes, ...val})
    }

    const handleClose = () => {
        props.setShow(false)
    }

    const handleConfirm = () => {
        const questiontoSend = {
            "id": 0,
            "answer1": ans1Field,
            "answer2": ans2Field,
            "answer3": ans3Field,
            "answer4": ans4Field,
            "answer5": ans5Field,
            "good_answer": toGoodAnswer(),
            "idMcq": props.mcqId,
            "question": question,
        }

        const successToast = () => {
            notify(
                `La question été ${props.questionToModif ? 'modifiée' : 'créée'} !`,
                'success'
            );
        }
        const warningToast = () => {
            notify("Un utilisateur a déja répondu à une question", 'error')
        };

        const action = props.questionToModif ? editQuestion(questiontoSend): postQuestion(questiontoSend);
        action.unwrap()
            .then((payload: any) => {
                successToast();
                handleClose();
            })
            .catch( err => {
                warningToast()
            });
    }

    const toGoodAnswer = () =>  {
        const gAnswers: string[] = [];
        Object.entries(checkboxes).forEach( (value, index) =>value[1]&& gAnswers.push(index + 1 + ''));
        return gAnswers.toString();
    }

    const content =
        <Box component="form" sx={{'& .MuiTextField-root': {m: 1, width: '50ch'},}} noValidate autoComplete="off">
            <TextField
                sx={inputFieldsStyle}
                required
                id="question"
                label="Intitulé"
                multiline
                rows={2}
                value={question}
                onChange={(e) => setQuestion(e.target.value)}
            />
            <TextField sx={inputFieldsStyle} required id="ans1Field" label="Réponse 1"
                       value={ans1Field}
                       onChange={(e) => setans1Field1(e.target.value)}
            />
            <Checkbox sx={checkBoxStyle}
                      checked={checkboxes.as1}
                      onChange={(e) => handleCheck({as1: e.target.checked})}
                      inputProps={{'aria-label': 'controlled'}}
            />

            <TextField sx={inputFieldsStyle} required id="ans2Field" label="Réponse 2"
                       value={ans2Field}
                       onChange={(e) => setans1Field2(e.target.value)}
            />
            <Checkbox sx={checkBoxStyle}
                      checked={checkboxes.as2}
                      onChange={(e) => handleCheck({as2: e.target.checked})}
                      inputProps={{'aria-label': 'controlled'}}
            />
            <TextField sx={inputFieldsStyle} required id="ans3Field" label="Réponse 3"
                       value={ans3Field}
                       onChange={(e) => setans1Field3(e.target.value)}
            />
            <Checkbox sx={checkBoxStyle}
                      checked={checkboxes.as3}
                      onChange={(e) => handleCheck({as3: e.target.checked})}
                      inputProps={{'aria-label': 'controlled'}}
            />
            <TextField sx={inputFieldsStyle} required id="ans4Field" label="Réponse 4"
                       value={ans4Field}
                       onChange={(e) => setans1Field4(e.target.value)}
            />
            <Checkbox sx={checkBoxStyle}
                      checked={checkboxes.as4}
                      onChange={(e) => handleCheck({as4: e.target.checked})}
                      inputProps={{'aria-label': 'controlled'}}
            />
            <TextField sx={inputFieldsStyle} required id="ans5Field" label="Réponse 5"
                       value={ans5Field}
                       onChange={(e) => setans1Field5(e.target.value)}
            />
            <Checkbox sx={checkBoxStyle}
                      checked={checkboxes.as5}
                      onChange={(e) => handleCheck({as5: e.target.checked})}
                      inputProps={{'aria-label': 'controlled'}}
            />
        </Box>

    const buttons =
        <div>
            <ThemeProvider theme={buttonTheme}>
                <Button variant="outlined" onClick={handleClose}>ANNULER</Button>
                <Button variant="contained" onClick={handleConfirm}>CONFIRMER</Button>
            </ThemeProvider>
        </div>

    return (
        <Popin open={props.show} handleClose={handleClose} title={props.title} content={content} buttons={buttons}/>
    )
}

export default CreateEditQuestionPopup;
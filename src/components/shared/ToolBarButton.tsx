import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import React from 'react';
import {IconDefinition} from '@fortawesome/free-solid-svg-icons';
import {FlatButton} from './FlatButton/FlatButton';

type ToolBarButtonprops = {
    icon: any,
    text: string,
    key: string,
    onClick: () => void,
    disabled?: boolean,
}

export const ToolBarButton = ({icon, text, onClick, disabled}: ToolBarButtonprops) => (
    <FlatButton
        onClick={onClick}
        disabled={disabled}
    >
        <FontAwesomeIcon icon={icon}/>
        {' '}
        <span className="responsive-hidden-mobile">{text}</span>
    </FlatButton>
)


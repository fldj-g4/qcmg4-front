import {styled, TextField} from '@mui/material';

const CssTextField = styled(TextField)({
    "& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {borderColor: "white" },
    "&:hover .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {borderColor: "white"},
    "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {borderColor: "white"},
    "& .MuiOutlinedInput-input": {color: "white"},
    "&:hover .MuiOutlinedInput-input": {color: "white"},
    "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-input": {color: "white"},
    "& .MuiInputLabel-outlined": {color: "white"},
    "&:hover .MuiInputLabel-outlined": {color: "white"},
    "& .MuiInputLabel-outlined.Mui-focused": {color: "white"}
});

export default CssTextField;
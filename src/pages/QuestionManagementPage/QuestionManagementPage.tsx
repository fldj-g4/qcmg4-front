import React from 'react';
import {Grid} from '@mui/material';
import QuestionManagement from '../../components/QuestionManagement/QuestionManagement';
import {useParams} from "react-router-dom"
import {useGetMcqByIdQuery} from "../../services/qcmplusApi";

const QuestionManagementPage = () => {
    return (
        <Grid container spacing={0}
              sx={{height: '90%', color: 'black'}}
        >
            <Grid
                item
                xs={15}
                p={5}
            >
                <QuestionManagement params={useParams()} />
            </Grid>
        </Grid>
    );
}
export default QuestionManagementPage;
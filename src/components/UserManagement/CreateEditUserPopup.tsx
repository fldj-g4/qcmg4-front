import {Popin} from '../shared/Popin/Popin'
import {Box, Button, Checkbox, FormControlLabel, TextField} from '@mui/material';
import {buttonTheme, checkBoxStyle, inputFieldsStyle} from '../shared/Styles';
import {ThemeProvider} from '@mui/material/styles';
import {useEffect, useState} from "react";
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import {useAuth} from "../../utils/hooks/useAuth";
import {
    LoginRequest,
    useAddUserMutation,
    useEditUserMutation,
    useGetUsersQuery,
    useLoginMutation
} from "../../services/qcmplusApi";
import {notify, roles} from "../../services/FunctionService";
import {toastify} from "../../utils/features/toastSlice";
import {setUsers} from "../../utils/features/modelSlice";
import {LoadingButton} from "@mui/lab";
import * as React from "react";
import {useAppDispatch} from "../../utils/hooks/store";

export const CreateEditUserPopup = (props: any) => {
    const {user: connectedUser} = useAuth();
    const [addUser, {isLoading: addLoading}] = useAddUserMutation();
    const [editUser, {isLoading: editLoading}] = useEditUserMutation();
    const [login, setLogin] = useState(props?.userToModif?.login || '');
    const [name, setName] = useState(props?.userToModif?.lastName || '');
    const [firstname, setFirstname] = useState(props?.userToModif?.firstName);
    // const [email, setEmail] = useState(props?.userToModif?.email || '');
    const [company, setCompany] = useState(props?.userToModif?.company || '');
    const [role, setRole] = useState(props?.userToModif?.role || {id: 3, name: 'USER'});
    const [isError, setIsError] = useState({
        login: false,
        name: false,
        firstname: false,
        // email: false,
        company: false,
    });

    const handleClose = () => {
        props.setShow(false)
    }
    const handleSetRole = (id: any) => {
        setRole(roles.find((rl) => rl.id === id) || {id: 3, name: 'USER'})
    }

    const isEmpty = (value: string) => {
        return value.replace(' ', '').length === 0;
    }

    const createOrEditUser = async () => {
        const userToSend = {
            id: props.userToModif?.id || 0,
            login,
            lastName: name,
            firstName: firstname,
            company,
            role: role
        }

        const successToast = ({login}: any) => {
            notify(
                `L'utilisateur ${login} a été ${props.userToModif ? 'modifié' : 'créé'} !`,
                'success'
            );
        }

        const warningToast = () => {
            notify("Une reur est survenue, veuillez réessayer plus tard", 'error')
        };
        const action = props.userToModif ? editUser(userToSend) : addUser(userToSend);
        action.unwrap()
            .then((payload) => {
                successToast(payload);
                handleClose();
            })
            .catch((err) => warningToast())
    }

    const handleConfirm = () => {
        const newErrors = {...isError};
        newErrors.login = isEmpty(login);
        newErrors.name = isEmpty(name);
        newErrors.firstname = isEmpty(firstname);
        newErrors.company = isEmpty(company);
        if (!Object.values(newErrors).find(val => val)) {
            createOrEditUser();
        } else {
            setIsError(newErrors);
        }
    }

    const content = (userToModif: any) => {
        return (
            <Box component="form" sx={{'& .MuiTextField-root': {m: 1, width: '35%'},}} noValidate autoComplete="off">
                <TextField sx={inputFieldsStyle}
                           required
                           id="loginField" label="Login"
                           defaultValue={userToModif.login || ""}
                           // onChange={(e) => setLogin(e.target.value.trim().replaceAll(' ', ''))}
                           // error={isError.login}
                           disabled={true}
                />
                <TextField sx={inputFieldsStyle}
                           required
                           id="nameField"
                           label="Nom"
                           defaultValue={userToModif.lastName || ""}
                           onChange={(e) => setName(e.target.value.trim())}
                           error={isError.name}
                />
                <TextField sx={inputFieldsStyle}
                           required id="firstNameField"
                           label="Prénom"
                           defaultValue={userToModif.firstName || ""}
                           onChange={(e) => setFirstname(e.target.value.trim())}
                           error={isError.firstname}
                />
                {/*<TextField sx={inputFieldsStyle}*/}
                {/*           required*/}
                {/*           id="emailField"*/}
                {/*           label="Adresse e-mail"*/}
                {/*           defaultValue={userToModif ? userToModif.email : ""}*/}
                {/*           onChange={(e) => setEmail(e.target.value.trim())}*/}
                {/*           error={isError.email}*/}
                {/*/>*/}
                <TextField sx={inputFieldsStyle}
                           required
                           id="societyField"
                           label="Société"
                           value={company || ""}
                           onChange={(e) => setCompany(e.target.value.trim())}
                           error={isError.company}
                />
                <Select
                    labelId="select-role"
                    id="select-role"
                    value={role?.id || 3}
                    label="Rôle"
                    onChange={(e) => handleSetRole(e.target.value)}
                    style={{
                        margin: '8px',
                        width: '35%',
                        borderColor: 'black'
                    }}
                    className="MuiSelect-outlined"
                >
                    {connectedUser?.role.id === 1 && <MenuItem value={1}>SUPERADMIN</MenuItem>}
                    <MenuItem value={2}>ADMIN</MenuItem>
                    <MenuItem value={3}>USER</MenuItem>
                </Select>
            </Box>
        )
    }

    const buttons =
        <div>
            <ThemeProvider theme={buttonTheme}>
                <Button variant="outlined" onClick={handleClose}>ANNULER</Button>
                <LoadingButton
                    sx={{marginLeft: 1}}
                    loading={addLoading || editLoading}
                    variant="contained"
                    onClick={handleConfirm}
                >
                    CONFIRMER
                </LoadingButton>
            </ThemeProvider>
        </div>

    return (
        <>
            <Popin open={props.show} handleClose={handleClose} title={props.title} content={content(props.userToModif)}
                   buttons={buttons}/>
        </>
    )
}

export default CreateEditUserPopup;
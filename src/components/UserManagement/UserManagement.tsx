import React, {useEffect, useState} from 'react';
import {Box, ButtonGroup, TextField} from '@mui/material';
import {Search} from '@mui/icons-material';
import InputAdornment from '@mui/material/InputAdornment';
import {faEye, faSyncAlt, faUserEdit, faUserMinus, faUserPlus} from '@fortawesome/free-solid-svg-icons'
import {Datatable} from '../shared/Datable/Datatable';
import {ToolBarButton} from '../shared/ToolBarButton';
import {OBJ, USER} from '../../model/types';
import {UsersManagementService} from '../../services/UserManagementService';
import {Loader} from '../shared/Loader';
import Divider from '@mui/material/Divider';
import {ErrorPopup} from '../shared/sharedPopup/ErrorPopup';
import {DeletePopup} from '../shared/sharedPopup/DeletePopup';
import {CreateEditUserPopup} from './CreateEditUserPopup';
import {DetailPopup} from '../shared/sharedPopup/DetailPopup';
import {useAuth} from "../../utils/hooks/useAuth";
import {useDeleteUsersMutation, useGetUsersQuery} from "../../services/qcmplusApi";

const columns = [
    {field: 'id', type: 'number', headerName: 'ID'},
    {
        field: 'login',
        headerName: 'Nom d\'utilisateur',
        sortable: true,
    },
    {
        field: 'firstName',
        headerName: 'Prénom',
        sortable: true,
        valueGetter: (params: any) => params.row.firstName || '',
    },
    {
        field: 'lastName',
        headerName: 'Nom',
        sortable: true,
        valueGetter: (params: any) => params.row.lastName || '',
    },
    {
        field: 'company',
        headerName: 'Entreprise',
        sortable: true,
        valueGetter: (params: any) => params.row.company || '',
    },
    {
        field: 'role',
        headerName: 'Rôle',
        sortable: true,
        width: 160,
        valueGetter: (params: any) => params.row.role.name + "toto",
    },
];

const UserManagement = () => {
    const {user} = useAuth();
    const [deleteUsers, {isLoading: deleteLoading}] = useDeleteUsersMutation();
    const {
        data: users,
        error,
        isLoading,
        refetch,
    } = useGetUsersQuery();
    const [selectedUsers, setSelectedUsers] = useState<any>([]);
    const [filteredUsers, setFilteredUsers] = useState(users);
    const [selectedUser, setSelectedUser] = useState<any>(null);
    const [isAddPopupOpen, setIsAddPopupOpen] = useState(false);
    const [isEditPopupOpen, setIsEditPopupOpen] = useState(false);
    const [isError0PopupOpen, setIsError0PopupOpen] = useState(false);
    const [isError2PopupOpen, setIsError2PopupOpen] = useState(false);
    const [isDeletePopupOpen, setIsDeletePopupOpen] = useState(false);
    const [isDetailPopupOpen, setIsDetailPopupOpen] = useState(false);

    const toggleCreatePopup = () => {
        setIsAddPopupOpen(!isAddPopupOpen)
    }

    const editUser = () => {
        switch (selectedUsers.length) {
            case 0 :
                setIsError0PopupOpen(!isError0PopupOpen);
                break
            case 1 :
                setIsEditPopupOpen(!isEditPopupOpen);
                setSelectedUser(selectedUsers[0]);
                break
            default :
                setIsError2PopupOpen(!isError2PopupOpen);
                break
        }
    }

    const deleteUser = () => {
        if (selectedUsers.length === 0) {
            setIsError0PopupOpen(!isError0PopupOpen);
        } else {
            setIsDeletePopupOpen(!isDeletePopupOpen);
        }
    }
    const detailUser = () => {
        switch (selectedUsers.length) {
            case 0 :
                setIsError0PopupOpen(!isError0PopupOpen);
                break
            case 1 :
                setIsDetailPopupOpen(!isDetailPopupOpen);
                setSelectedUser(selectedUsers[0]);
                break
            default :
                setIsError2PopupOpen(!isError2PopupOpen);
                break
        }
    }

    const filterUser = (val: string) => {
        if (!users) return;
        if (val.replaceAll(' ', '').length === 0) {
            setFilteredUsers(users);
        } else {
            const criteria = val.trim();
            setFilteredUsers(
                users.filter(user => {
                    return `${user.id}` === criteria ||
                        user.login?.includes(criteria) ||
                        user.lastName?.includes(criteria) ||
                        user.firstName?.includes(criteria) ||
                        user.company?.includes(criteria) ||
                        user.role?.name?.includes(criteria)
                })
            );
        }
    }

    useEffect(() => {
        if (users) {
            setFilteredUsers(users)
        }
    }, [users]);

    users?.length === 0 && filterUser('')
    return (
        <div>q
            <Box sx={{
                boxShadow: 'var(--cst-shadow)',
                height: '100%',
                backgroundColor: 'var(--cst-white)',
            }}
            >
                <ButtonGroup size="large" sx={{
                    width: '100%',
                    borderRadius: '0',
                    borderBottom: '1px solid grey',
                    '> button': {flexGrow: 1},
                }}>
                    <ToolBarButton key="one" icon={faSyncAlt} text="RAFRAÎCHIR" onClick={() => refetch()}/>
                    <Divider orientation="vertical" flexItem/>
                    <ToolBarButton key="two" icon={faEye} text="CONSULTER" onClick={() => detailUser()}/>
                    <Divider orientation="vertical" flexItem/>
                    <ToolBarButton key="three" icon={faUserPlus} text="AJOUTER" onClick={() => toggleCreatePopup()}/>
                    <Divider orientation="vertical" flexItem/>
                    <ToolBarButton key="viva" icon={faUserEdit} text="MODIFIER" onClick={() => editUser()}/>
                    <Divider orientation="vertical" flexItem/>
                    <ToolBarButton key="lalgérie" icon={faUserMinus} text="SUPPRIMER" onClick={() => deleteUser()}/>
                </ButtonGroup>

                <TextField
                    sx={{
                        'svg': {color: 'grey'},
                        width: '100%',
                    }}
                    variant="standard"
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <Search/>
                            </InputAdornment>
                        ),
                    }}
                    onChange={(e) => filterUser(e.target.value)}
                />

                {
                    (isLoading) ? <Loader/> :
                        <Datatable
                            entities={filteredUsers || []}
                            columns={columns}
                            selectEntities={(entities: USER[]) => {
                                setSelectedUsers(entities)
                            }}
                            columnTranslations={UsersManagementService.getTranslationMapping()}
                        />
                }
            </Box>
            {
                !isLoading && <>
                    <DetailPopup
                        show={isDetailPopupOpen}
                        setShow={(show: boolean) => setIsDetailPopupOpen(show)}
                        title={(selectedUser !== undefined) ? "Détails de l'utilisateur " + selectedUser?.login : ""}
                        entityToDetail={selectedUser}
                        columnTranslations={UsersManagementService.getTranslationMapping()}>
                    </DetailPopup>
                    {
                        !selectedUser &&
                        <CreateEditUserPopup
                            show={isAddPopupOpen}
                            setShow={(show: boolean) => setIsAddPopupOpen(show)}
                            title="Ajouter un nouvel utilisateur">
                        </CreateEditUserPopup>
                    }
                    {
                        selectedUser && <CreateEditUserPopup
                            show={isEditPopupOpen}
                            setShow={(show: boolean) => setIsEditPopupOpen(show)}
                            title={selectedUser && "Édition de l'utilisateur " + selectedUser?.login || ''}
                            userToModif={selectedUser}>
                        </CreateEditUserPopup>
                    }

                    <ErrorPopup
                        show={isError0PopupOpen}
                        setShow={(show: boolean) => setIsError0PopupOpen(show)}
                        content={"Veuillez sélectionner au moins un utilisateur avant de choisir cette option."}>
                    </ErrorPopup>
                    <ErrorPopup
                        show={isError2PopupOpen}
                        setShow={(show: boolean) => setIsError2PopupOpen(show)}
                        content={"Veuillez ne sélectionner qu'un seul utilisateur avant de choisir cette option."}>
                    </ErrorPopup>
                    <DeletePopup
                        show={isDeletePopupOpen}
                        setShow={(show: boolean) => setIsDeletePopupOpen(show)}
                        entitiesToDel={selectedUsers}
                        entityName={"utilisateur"}>
                        deleteCallBack={(users: any) => deleteUsers(users)}
                    </DeletePopup>
                </>
            }
        </div>
    )
}

export default UserManagement;


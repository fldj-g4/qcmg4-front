import React, {useState} from 'react';
import {Box, ButtonGroup, TextField} from '@mui/material';
import {Search} from '@mui/icons-material';
import InputAdornment from '@mui/material/InputAdornment';
import {faEye, faPen, faPlusCircle, faSyncAlt, faTrash} from '@fortawesome/free-solid-svg-icons'
import {Datatable} from '../shared/Datable/Datatable';
import {OBJ, QUESTION} from '../../model/types';
import {QuestionManagementService} from '../../services/QuestionManagementService';
import {CreateEditQuestionPopup} from './CreateEditQuestionPopup';
import {Loader} from '../shared/Loader';
import {ToolBarButton} from '../shared/ToolBarButton';
import Divider from '@mui/material/Divider';
import {ErrorPopup} from '../shared/sharedPopup/ErrorPopup';
import {DeletePopup} from '../shared/sharedPopup/DeletePopup';
import {DetailPopup} from '../shared/sharedPopup/DetailPopup';
import {useDeleteQuestionMutation, useGetMcqByIdQuery, useGetUsersQuery} from "../../services/qcmplusApi";


const columns = [
    {field: 'question', headerName: 'Intitulé'},
    {field: 'answer1', headerName: 'Réponse 1'},
    {field: 'answer2', headerName: 'Réponse 2'},
    {field: 'answer3', headerName: 'Réponse 3'},
    {field: 'answer4', headerName: 'Réponse 4'},
    {field: 'answer5', headerName: 'Réponse 5'},
    {field: 'good_answer', headerName: 'Bonnes réponses'},
]

type QuestionManagementStates = {
    selectedEntities: OBJ[],
    questions: OBJ[],
    isLoaded: boolean,
    isAddPopupOpen: boolean,
    isEditPopupOpen: boolean,
    selectedQuestion: Partial<QUESTION>,
    isError0PopupOpen: boolean,
    isError2PopupOpen: boolean,
    isDeletePopupOpen: boolean,
    isDetailPopupOpen: boolean
}

const QuestionManagement = ({params}: any) => {
    const mcqId = params?.id;
    const {data, error, isLoading, refetch} = useGetMcqByIdQuery(params?.id);
    const questions = data?.questions
    const [deleteQuestion, {isLoading: deleteLoader}] = useDeleteQuestionMutation();
    const [selectedEntities, setSelectedEntities] = useState(questions || []);
    const [isAddPopupOpen, setIsAddPopupOpen] = useState(false);
    const [isEditPopupOpen, setIsEditPopupOpen] = useState(false);
    const [selectedQuestion, setSelectedQuestion] = useState({});
    const [isError0PopupOpen, setIsError0PopupOpen] = useState(false);
    const [isError2PopupOpen, setIsError2PopupOpen] = useState(false);
    const [isDeletePopupOpen, setIsDeletePopupOpen] = useState(false);
    const [isDetailPopupOpen, setIsDetailPopupOpen] = useState(false);

    const refresh = async () => {
        refetch();
    }

    const toggleCreatePopup = () => {
        setIsAddPopupOpen(!isAddPopupOpen);
    }

    const editQuestion = (selectedEntities : OBJ[]) => {
        switch (selectedEntities.length) {
            case 0 :
                setIsError0PopupOpen(!isError0PopupOpen)
                break
            case 1 :
                setIsEditPopupOpen(!isEditPopupOpen);
                setSelectedQuestion(selectedEntities[0]);
                break
            default :
                setIsError2PopupOpen(!isError2PopupOpen)
                break
        }
    }

    const handleDeleteQuestion = (selectedEntities : OBJ[]) => {
        selectedEntities.length === 0 ? setIsDeletePopupOpen(!isError0PopupOpen) : setIsDeletePopupOpen(!isDeletePopupOpen);
    }

   const detailQuestion = (selectedEntities : OBJ[]) => {
        switch (selectedEntities.length) {
            case 0 :
                setIsError0PopupOpen(!isError0PopupOpen);
                break
            case 1 :
                setIsDetailPopupOpen(!isDetailPopupOpen);
                setSelectedQuestion(selectedEntities[0]);
                break
            default :
                setIsError2PopupOpen(!isError2PopupOpen)
                break
        }
    }

    return (
        <div>
            <Box sx={{
                boxShadow: 'var(--cst-shadow)',
                height: '100%',
                backgroundColor: 'var(--cst-white)',
            }}>
                <ButtonGroup size="large" sx={{
                    width: '100%',
                    borderRadius: '0',
                    borderBottom: '1px solid grey',
                    '> button': {flexGrow: 1},
                }}>
                    <ToolBarButton key="refresh" icon={faSyncAlt} text="RAFRAÎCHIR" onClick={() => refresh()}/>
                    <Divider orientation="vertical" flexItem />
                    <ToolBarButton key="two" icon={faEye} text="CONSULTER" onClick={() => detailQuestion(selectedEntities)}/>
                    <Divider orientation="vertical" flexItem />
                    <ToolBarButton key="add" icon={faPlusCircle} text="AJOUTER" onClick={() => toggleCreatePopup()}/>
                    <Divider orientation="vertical" flexItem />
                    <ToolBarButton key="edit_mcq" icon={faPen} text="MODIFIER" onClick={() => editQuestion(selectedEntities)}/>
                    <Divider orientation="vertical" flexItem />
                    <ToolBarButton key="del" icon={faTrash} text="SUPPRIMER" onClick={() => handleDeleteQuestion(selectedEntities)}/>
                </ButtonGroup>

                <TextField
                    sx={{
                        'svg': {color: 'grey'},
                        width: '100%',
                    }}
                    variant="standard"
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <Search/>
                            </InputAdornment>
                        ),
                    }}
                />

                {
                    ( isLoading ) ? <Loader/> :
                        <Datatable
                            entities={questions}
                            selectEntities={(entities : OBJ[]) => setSelectedEntities(entities)}
                            columnTranslations={QuestionManagementService.getTranslationMapping()}
                            columns={columns}
                        />
                }
            </Box>

            {
                !isLoading && <>
                    <DetailPopup
                        show={isDetailPopupOpen}
                        setShow={(show:boolean) => setIsDetailPopupOpen(show)}
                        title={(selectedQuestion !== undefined) ? "Détails de la question " + (questions?.indexOf(selectedQuestion) + 1): ""}
                        entityToDetail = {selectedQuestion}
                        columnTranslations={QuestionManagementService.getTranslationMapping()}>
                    </DetailPopup>
                    <CreateEditQuestionPopup
                        show={isAddPopupOpen}
                        setShow={(show:boolean) => setIsAddPopupOpen(show)}
                        mcqId={mcqId}
                        title="Ajouter une nouvelle question">
                    </CreateEditQuestionPopup>
                    <CreateEditQuestionPopup
                        show={isEditPopupOpen}
                        setShow={(show:boolean) => setIsEditPopupOpen(show)}
                        title={(selectedQuestion !== undefined) ? "Édition de la question " + (questions?.indexOf(selectedQuestion) + 1) : ""}
                        questionToModif = {selectedQuestion}>
                    </CreateEditQuestionPopup>
                    <ErrorPopup
                        show={isError0PopupOpen}
                        setShow={(show:boolean) => setIsError0PopupOpen(show)}
                        content={"Veuillez sélectionner au moins une question avant de choisir cette option."}>
                    </ErrorPopup>
                    <ErrorPopup
                        show={isError2PopupOpen}
                        setShow={(show:boolean) => setIsError2PopupOpen(show)}
                        content={"Veuillez ne sélectionner qu'une seule question avant de choisir cette option."}>
                    </ErrorPopup>
                    <DeletePopup
                        show={isDeletePopupOpen}
                        setShow={(show:boolean) => setIsDeletePopupOpen(show)}
                        entitiesToDel={selectedEntities}
                        entityName = {"question"}
                        questionList = {questions}
                        deleteCallBack={deleteQuestion}
                    />
                </>
            }
        </div>
    );

}

export default QuestionManagement;

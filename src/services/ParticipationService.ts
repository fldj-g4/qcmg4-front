import {OBJ, PARTICIPATION} from '../model/types';

export const ParticipationService = {
    seedObj: () => seedObj(),
    getTranslationMapping: () => getTranslationMapping(),
}
const seed = [
    [
        {
            id : "0",
            date : "21/08/2021",
            duration : 100
        },
        {
            id : "1",
            date : "30/10/2021",
            duration : 1000
        },
        {
            id : "2",
            date : "13/12/2021",
            duration : 12000
        }
    ],
    [
        {
            id : "3",
            date : "01/01/2022",
            duration : 30000
        },
        {
            id : "4",
            date : "21/01/2022",
            duration : 1000
        },
        {
            id : "5",
            date : "25/01/2022",
            duration : 12000
        }
    ]
]

const seedObj = (): Promise<PARTICIPATION[][]> => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            return resolve(seed);
        }, 2000);
    });
}

const getTranslationMapping = (): OBJ => {
    return {
        duration: 'durée',
    }
}

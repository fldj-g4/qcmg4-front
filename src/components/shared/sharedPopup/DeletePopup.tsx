import {Popin} from '../Popin/Popin'
import {Button, List, ListItem, ListItemText} from '@mui/material';
import {OBJ} from '../../../model/types';
import {ThemeProvider} from '@mui/material/styles';
import {buttonTheme} from '../Styles';
import {useAppDispatch} from "../../../utils/hooks/store";
import {useDeleteUsersMutation} from "../../../services/qcmplusApi";
import {toastify} from "../../../utils/features/toastSlice";

export const DeletePopup = (props: any) => {
    const dispatch = useAppDispatch()

    const handleClose = () => {
        props.setShow(false)
    }

    const handleConfirm = async () => {
        const successToast = () => {
            dispatch(toastify({
                open: true,
                message: `La suppression a bien été effectuée !`,
                severity: 'success'
            }))
        };

        const warningToast = (error: any) => {
            dispatch(toastify({
                open: true,
                message: error.message,
                severity: 'error'
            }))
        };
        await props.deleteCallBack(props.entitiesToDel).unwrap()
            .then(() => successToast())
            .catch((err: any) => warningToast(err))
            .finally(() => props.setShow(false))
    }

    const buttons =
        <div>
            <ThemeProvider theme={buttonTheme}>
                <Button variant="outlined" onClick={handleClose}>ANNULER</Button>
                <Button variant="contained" color="warning" onClick={handleConfirm}>CONFIRMER LA SUPPRESSION</Button>
            </ThemeProvider>
        </div>

    const titleOne = (props: any) => {
        switch (props.entityName) {
            case 'questionnaire' :
                return `Suppression du questionnaire ${props.entitiesToDel[0].name}`;
            case 'utilisateur' :
                return `Suppression de l'utilisateur ${props.entitiesToDel[0].login}`;
            case 'question' :
                return `Suppression de la question ${props.questionList.indexOf(props.entitiesToDel[0]) + 1}`;
            default :
                return '';
        }
    }

    const contentOne = (props: any) => {
        switch (props.entityName) {
            case 'questionnaire' :
                return <div>
                    <p>Attention, vous vous apprêtez à supprimer le questionnaire suivant :</p>
                    <List sx={{
                        width: '100%',
                        bgcolor: 'background.paper',
                        position: 'relative',
                        overflow: 'auto',
                        maxHeight: 100,
                        '& ul': {padding: 0},
                    }} subheader={<li/>}>
                        {props.entitiesToDel.map((entity: OBJ) => (
                            <ListItem>
                                <ListItemText primary={entity.name + " : " + (entity.description || entity.question)}/>
                            </ListItem>
                        ))}
                    </List>
                    <p>Êtes-vous sûr de vouloir le supprimer ?</p>
                </div>;
            case 'utilisateur' :
                return <div>
                    <p>Attention, vous vous apprêtez à supprimer l'utilisateur suivant :</p>
                    <List sx={{
                        width: '100%',
                        bgcolor: 'background.paper',
                        position: 'relative',
                        overflow: 'auto',
                        maxHeight: 100,
                        '& ul': {padding: 0},
                    }} subheader={<li/>}>
                        {props.entitiesToDel.map((entity: OBJ) => (
                            <ListItem>
                                <ListItemText
                                    primary={entity.login + " : " + entity.firstName + " " + entity.lastName + " de la société " + entity.company}/>
                            </ListItem>
                        ))}
                    </List>
                    <p>Êtes-vous sûr de vouloir le supprimer ?</p>
                </div>;
            case 'question' :
                return <div>
                    <p>Attention, vous vous apprêtez à supprimer la question suivante :</p>
                    <List sx={{
                        width: '100%',
                        bgcolor: 'background.paper',
                        position: 'relative',
                        overflow: 'auto',
                        maxHeight: 100,
                        '& ul': {padding: 0},
                    }} subheader={<li/>}>
                        {props.entitiesToDel.map((entity: OBJ) => (
                            <ListItem>
                                <ListItemText
                                    primary={"Question " + (props.questionList.indexOf(entity) + 1) + " : " + (entity.title || entity.question)}/>
                            </ListItem>
                        ))}
                    </List>
                    <p>Êtes-vous sûr de vouloir la supprimer ?</p>
                </div>;
            default :
                return <div></div>;
        }
    }

    const contentMany = (props: any) => {
        switch (props.entityName) {
            case 'questionnaire' :
                return <div>
                    <p>Attention, vous vous apprêtez à supprimer les {props.entitiesToDel.length} questionnaires
                        suivants :</p>
                    <List sx={{
                        width: '100%',
                        bgcolor: 'background.paper',
                        position: 'relative',
                        overflow: 'auto',
                        maxHeight: 100,
                        '& ul': {padding: 0},
                    }} subheader={<li/>}>
                        {props.entitiesToDel.map((entity: OBJ) => (
                            <ListItem>
                                <ListItemText primary={entity.name + " : "}/>
                            </ListItem>
                        ))}
                    </List>
                    <p>Êtes-vous sûr de vouloir les supprimer ?</p>
                </div>;
            case 'utilisateur' :
                return <div>
                    <p>Attention, vous vous apprêtez à supprimer les {props.entitiesToDel.length} utilisateurs suivants
                        :</p>
                    <List sx={{
                        width: '100%',
                        bgcolor: 'background.paper',
                        position: 'relative',
                        overflow: 'auto',
                        maxHeight: 100,
                        '& ul': {padding: 0},
                    }} subheader={<li/>}>
                        {props.entitiesToDel.map((entity: OBJ) => (
                            <ListItem>
                                <ListItemText
                                    primary={entity.login + " : " + entity.firstName + " " + entity.lastName + " de la société " + entity.company}/>
                            </ListItem>
                        ))}
                    </List>
                    <p>Êtes-vous sûr de vouloir les supprimer ?</p>
                </div>
            case 'question' :
                return <div>
                    <p>Attention, vous vous apprêtez à supprimer les {props.entitiesToDel.length} questions suivantes
                        :</p>
                    <List sx={{
                        width: '100%',
                        bgcolor: 'background.paper',
                        position: 'relative',
                        overflow: 'auto',
                        maxHeight: 100,
                        '& ul': {padding: 0},
                    }} subheader={<li/>}>
                        {props.entitiesToDel.map((entity: OBJ) => (
                            <ListItem>
                                <ListItemText
                                    primary={"Question " + (props.questionList.indexOf(entity) + 1) + " : " + entity.title}/>
                            </ListItem>
                        ))}
                    </List>
                    <p>Êtes-vous sûr de vouloir les supprimer ?</p>
                </div>
            default :
                return <div/>;
        }
    }
    return (
        <div>
            {props.entitiesToDel.length === 1
                ? <Popin open={props.show} handleClose={handleClose} title={titleOne(props)} buttons={buttons}
                         content={contentOne(props)}/>
                : <Popin open={props.show} handleClose={handleClose}
                         title={`Suppression de ${props.entitiesToDel.length} ${props.entityName}s`} buttons={buttons}
                         content={contentMany(props)}/>
            }
        </div>
    )
}

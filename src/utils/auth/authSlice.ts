import {createSlice} from "@reduxjs/toolkit";
import {LOADER, USER} from "../../model/types";
import {qcmplusApi} from "../../services/qcmplusApi";
import {RootState} from "../../store";
import ls from "localstorage-slim";

interface UserState {
    user: USER | null,
    userLoader: LOADER,
    token: string | null,
}

const authSlice = createSlice({
    name: 'auth',
    initialState: {
        user: null,
        userLoader: 'born',
        token: null,
    } as UserState,
    reducers: {
        disconnect(state) {
            state.token = '';
            state.user = null;
        },
        updateToken(state, {payload}: any) {
            state.token = payload.token;
        }
    },
    extraReducers: (builder) => {
        const {login, getUserByToken} = qcmplusApi.endpoints;
        builder.addMatcher(
            login.matchFulfilled,
            (state, {payload}) => {
                const {token, user} = payload;
                state.token = token
                state.user = user
                state.userLoader = 'idle'
            }
        );
    },
});

export const {disconnect, updateToken} = authSlice.actions;
export default authSlice.reducer;

export const selectCurrentUser = (state: RootState) => ({user: state.auth.user, userLoader: state.auth.userLoader});
export const selectToken = (state: RootState) => (state.auth.token);

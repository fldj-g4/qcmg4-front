import {createTheme} from '@mui/material/styles';

const inputFieldsStyle = {
    "& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {borderColor: "black" },
    "& .MuiInputBase-root": {borderColor: "black" },
    "&:hover .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {borderColor: "black"},
    "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {borderColor: "black"},
    "& .MuiOutlinedInput-input": {color: "black"},
    "&:hover .MuiOutlinedInput-input": {color: "black"},
    "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-input": {color: "black"},
    "& .MuiInputLabel-outlined": {color: "black"},
    "&:hover .MuiInputLabel-outlined": {color: "black"},
    "& .MuiInputLabel-outlined.Mui-focused": {color: "black"},
}

const buttonTheme = createTheme({
    palette: {
      primary: {
        main: '#3F51B5',
      },
      warning: {
        main: '#c14d5e'
      }
    },
  });

const checkBoxStyle = {
    color: "#424242",
    '&.Mui-checked': {color: '#3F51B5'},
    margin : 1
}

export { inputFieldsStyle , buttonTheme , checkBoxStyle }
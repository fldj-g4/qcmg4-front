import Modal from '@mui/material/Modal';
import Backdrop from '@mui/material/Backdrop';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';

type PopinProps = {
    open: boolean,
    handleClose: () => void,
    title: string,
    content: any,
    buttons: any
}

export const Popin = ({open, handleClose, title, content, buttons}: PopinProps) => {
    return <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="parent-modal-title"
        aria-describedby="parent-modal-description"
        BackdropComponent={Backdrop}
        BackdropProps={{
            timeout: 500,
        }}
    >
        <Card sx={style}>
            <CardHeader
                action={
                    <IconButton sx={{top: -10, color : "#FFFFFF"}} aria-label="close" onClick={handleClose}>
                        <CloseIcon />
                    </IconButton>
                }
                title={title}
                sx={{height: 10, color : "#FFFFFF", bgcolor: "#545763"}}
            />

            <CardContent>{content}
            </CardContent>

            <CardActions sx={{justifyContent: 'flex-end'}}>
                {buttons}
            </CardActions>
        </Card>
    </Modal>
}

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    width: '50%',
    maxWidth: '100vw',
    maxHeight: '100%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'background.paper',
    boxShadow: 24,
};

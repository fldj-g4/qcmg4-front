import {Box, Icon} from '@mui/material';
import * as React from 'react';
import {useNavigate} from 'react-router-dom';

type NavigationBarElementProps = {
    icon: string
    path?: string
    text: string
    onClick?: () => void
}

export const NavigationBarElement = ({icon, text, path, onClick}: NavigationBarElementProps) => {
    const navigate = useNavigate();
    const isActive = window.location.pathname === path;
    return (
        <Box sx={{textAlign: 'center'}}>
            <div
                aria-label="navigate"
                onClick={() => onClick ? onClick() : navigate(path || '/')}
                className={`navigation-element ${isActive ? 'active' : ''}`}
            >
                <div>
                    <Icon>{icon}</Icon>
                </div>
                {text}
            </div>
        </Box>
    )
};

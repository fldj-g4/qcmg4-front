import {Popin} from '../Popin/Popin'
import Button from '@mui/material/Button';
import {ThemeProvider} from '@mui/material/styles';
import {buttonTheme} from '../Styles'

export const ErrorPopup = (props:any) => {

    const handleClose = () => {
        props.setShow(false)
      }

    const content = props.content
    
    const buttons = <ThemeProvider theme={buttonTheme}><Button variant="contained" onClick={handleClose}>RETOUR</Button></ThemeProvider>

    return (
        <Popin open={props.show} handleClose={handleClose} title={"Erreur"} content={content} buttons={buttons}></Popin>
      )
}
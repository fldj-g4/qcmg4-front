import {MCQ, OBJ} from '../model/types';

export const McqManagementService = {
    seedObj: () => seedObj(),
    getTranslationMapping: () => getTranslationMapping(),
}

const seedObj = (): Promise<MCQ[]> => {
    const seed = [
        {
            id: "0",
            name: "QCM1",
            description: "Pour les champions",
            created_by:"Walter White",
            last_edited_by:"Peter Parkinson",
            created_at: "12/12/21",
            last_edited_at: "13/12/21",
            isActive: true,
        },
        {
            id: "1",
            name: "QCM2",
            description: "Pour les nuls",
            created_by:"Peter Parker",
            last_edited_by:"Johnny M",
            created_at: "20/05/20",
            last_edited_at: "12/06/21",
            isActive: true,
        },
        {
            id: "2",
            name: "QCM3",
            description: "Pour les pas trop nuls mais un peu quand même",
            created_by:"Will Smith",
            last_edited_by:"Michael Phelps",
            created_at: "01/01/19",
            last_edited_at: "02/01/20",
            isActive: true,
        },
        {
            id: "3",
            name: "QCM4",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            created_by:"Un vieux type mort",
            last_edited_by:"Un autre type mort",
            created_at: "01/01/01",
            last_edited_at: "02/01/01",
            isActive: false,
        },
    ]
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            return resolve(seed);
        }, 2000);
    });
}

const getTranslationMapping = (): OBJ => {
    return {
        name: 'nom',
        createdBy: 'créé par',
        lastEditionBy: 'modifié par',
        createdAt: 'créé le',
        lastEditionAt: 'modifié le',
        isActive: 'actif ?',
    }
}
